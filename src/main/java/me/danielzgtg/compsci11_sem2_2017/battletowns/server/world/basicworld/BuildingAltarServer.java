package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import java.util.Collections;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

public final class BuildingAltarServer extends BuildingServer<Void> {

	private int winTicks = BattleTownConstants.WIN_TICKS;

	public BuildingAltarServer(final TileServer tile, final PlayerServer owner) {
		super(tile, BattleTownConstants.HEALTH_ALTAR, BattleTownConstants.WEAPON_TYPE_SACRED, owner);

		Validate.notNull(owner);
	}

	@Override
	public final StringBuilder getAdornmentData() {
		return new StringBuilder().append(this.winTicks);
	}

	@Override
	public final void tickAdornment() {
		final PlayerServer owner = this.owner;
		final int newFaith = owner.getResource(Resource.FAITH) - BattleTownConstants.UPKEEP_ALTAR_FAITH;

		if (newFaith < 0) {
			this.kill();
			return;
		}

		if (--this.winTicks == 0) {
			owner.grantVictory();
		} else {
			owner.setResource(Resource.FAITH, newFaith);
			this.getTile().buildingModifiedEvent.fire(null);
		}
	}

	@Override
	public final boolean canAdornTile(final TileServer tile) {
		return tile instanceof TileSacredServer;
	}

	@Override
	public List<String> getAdornmentActions() {
		return Collections.emptyList();
	}

	@Override
	protected void doPerformAction(final String action) {
		throw new AssertionError();
	}

	@Override
	protected final Void startBattle() {
		return null;
	}

	@Override
	protected final void endBattle(final Void battleData) {}

	@Override
	public final int getAttackAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return BattleTownConstants.ATTACK_ALTAR;
	}

	@Override
	public final int getDefenseAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return BattleTownConstants.DEFENSE_ALTAR;
	}

	@Override
	public final int getAttackPriority(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return Integer.MIN_VALUE;
	}

	@Override
	public void advanceBattle(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {}
}
