package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.UnitServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;

public class UnitPriestServer extends UnitServer<Void> {

	public static final List<String> ACTIONS = Collections.unmodifiableList(Arrays.asList("altar"));

	protected UnitPriestServer(final TileServer tile, final PlayerServer owner) {
		super(tile, BattleTownConstants.HEALTH_PRIEST, BattleTownConstants.WEAPON_TYPE_SACRED, owner);
	}

	@Override
	public void tickAdornment() {
		final int newFood = this.owner.getResource(Resource.FOOD) - BattleTownConstants.UPKEEP_PRIEST_FOOD;

		if (newFood > 0) {
			this.owner.setResource(Resource.FOOD, newFood);
		} else {
			this.owner.sendChat(BattleTownConstants.ACTIONMSG_UPKEEP_UNMET);

			this.kill();
			return;
		}

		this.owner.setResource(Resource.FAITH,
				this.owner.getResource(Resource.FAITH) + BattleTownConstants.GENERATION_PRIEST_FAITH);
	}

	@Override
	public boolean canAdornTile(final TileServer tile) {
		return true;
	}

	@Override
	protected List<String> getAdornmentActions() {
		return ACTIONS;
	}

	@Override
	protected void doPerformAction(final String action) {
		final PlayerServer owner = this.owner;
		if (owner == null) {
			throw new AssertionError();
		}

		final TileServer<?> tile = this.getTile();
		final BuildingServer building = tile.getBuilding();
		if ("altar".equals(action)) {
			if (building != null) {
				owner.sendChat(BattleTownConstants.ACTIONMSG_BUILDING_NOTEMPTY);
				return;
			}

			final int newFaith = this.owner.getResource(Resource.FAITH) - BattleTownConstants.PURCHASE_ALTAR_FAITH;

			if (newFaith < 0) {
				this.owner.sendChat(BattleTownConstants.ACTIONMSG_MORE_RESOURCES);
				return;
			}

			this.owner.setResource(Resource.FAITH, newFaith);

			tile.changeBuilding((tile2) ->
					new BuildingScaffoldServer(tile2, BuildingAltarServer.class,
							BattleTownConstants.BUILDTICKS_ALTAR, owner));
		} else {
			throw new AssertionError();
		}
	}

	@Override
	public StringBuilder getAdornmentData() {
		return new StringBuilder();
	}

	@Override
	protected Void startBattle() {
		return null;
	}

	@Override
	protected void endBattle(final Void battleData) {}

	@Override
	public int getAttackAgainst(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return BattleTownConstants.ATTACK_PRIEST;
	}

	@Override
	public int getDefenseAgainst(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return BattleTownConstants.DEFENSE_PRIEST;
	}

	@Override
	public void advanceBattle(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {}

	@Override
	public int getAttackPriority(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return isAttacker ? BattleTownConstants.PRIORITY_SACRED : Integer.MIN_VALUE;
	}
}
