package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore;

import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;

public abstract class BuildingServer<B> extends TileAdornment<B> {

	protected BuildingServer(final TileServer tile,
			final int initialHealth, final String weaponType, final PlayerServer owner) {
		super(tile, false, initialHealth, weaponType, owner);
	}
}
