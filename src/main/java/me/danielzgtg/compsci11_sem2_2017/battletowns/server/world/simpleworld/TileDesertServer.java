package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.simpleworld;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.TileBarrenServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;

public final class TileDesertServer extends TileServer<Void> {

	public TileDesertServer(final int x, final int y, final WorldServer world) {
		super(x, y, world, BattleTownConstants.HEALTH_DESERT, BattleTownConstants.WEAPON_TYPE_TERRAIN);
	}

	@Override
	public final void tileTick() {}

	@Override
	public final TileServer getReplacement() {
		return new TileBarrenServer(this.getX(), this.getY(), this.world);
	}

	@Override
	protected final Void startBattle() {
		return null;
	}

	@Override
	protected final void endBattle(final Void battleData) {}

	@Override
	public final int getAttackAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return BattleTownConstants.ATTACK_DESERT;
	}

	@Override
	public final int getDefenseAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return BattleTownConstants.DEFENSE_DESERT;
	}

	@Override
	public final int getAttackPriority(final WorldObject other, final boolean isAttacker,
			final Void battleData, final int sufferedDamage) {
		return BattleTownConstants.PRIORITY_TERRAIN;
	}

	@Override
	public void advanceBattle(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {}

	@Override
	public final StringBuilder getPartData() {
		return new StringBuilder();
	}
}
