package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;

public class TileSacredServer extends TileServer<Void> {

	public TileSacredServer(final int x, final int y, final WorldServer world) {
		super(x, y, world, Integer.MAX_VALUE, BattleTownConstants.WEAPON_TYPE_SACRED);
	}

	@Override
	public final void tileTick() {}

	@Override
	public final TileServer getReplacement() {
		return new TileSacredServer(this.getX(), this.getY(), this.world);
	}

	@Override
	protected final Void startBattle() {
		return null;
	}

	@Override
	protected final void endBattle(final Void battleData) {}

	@Override
	public final int getAttackAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return other instanceof BuildingAltarServer ? 0 : BattleTownConstants.ATTACK_ALTAR;
	}

	@Override
	public final int getDefenseAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return Integer.MAX_VALUE;
	}

	@Override
	public final int getAttackPriority(final WorldObject other, final boolean isAttacker,
			final Void battleData, final int sufferedDamage) {
		final BuildingServer building = this.getBuilding();

		return building != null && other.owner == building.owner ?
				Integer.MIN_VALUE : BattleTownConstants.PRIORITY_SACRED;
	}

	@Override
	public void advanceBattle(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {}

	@Override
	public final StringBuilder getPartData() {
		return new StringBuilder();
	}
}
