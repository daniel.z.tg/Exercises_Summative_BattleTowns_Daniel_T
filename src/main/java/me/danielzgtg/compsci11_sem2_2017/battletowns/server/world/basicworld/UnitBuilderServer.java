package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.UnitServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;

public class UnitBuilderServer extends UnitServer<int[]> {

	public static final List<String> ACTIONS = Collections.unmodifiableList(Arrays.asList(
			"demolish", "farm", "barracks"));

	public UnitBuilderServer(final TileServer tile, final PlayerServer owner) {
		super(tile, BattleTownConstants.HEALTH_BUILDER, BattleTownConstants.WEAPON_TYPE_CIVIL, owner);
	}

	@Override
	public StringBuilder getAdornmentData() {
		return new StringBuilder();
	}

	@Override
	public void tickAdornment() {}

	@Override
	public boolean canAdornTile(final TileServer tile) {
		return true;
	}

	@Override
	protected List<String> getAdornmentActions() {
		return ACTIONS;
	}

	@Override
	protected void doPerformAction(final String action) {
		final PlayerServer owner = this.owner;
		if (owner == null) {
			throw new AssertionError();
		}

		final TileServer<?> tile = this.getTile();
		final BuildingServer building = tile.getBuilding();
		if ("demolish".equals(action)) {
			if (building == null) {
				owner.sendChat(BattleTownConstants.ACTIONMSG_DEMOLISH_NOTARGET);
				return;
			}

			if (!owner.equals(building.getOwner())) {
				owner.sendChat(BattleTownConstants.ACTIONMSG_DEMOLISH_NOTOWNER);
				return;
			}

			owner.setResource(Resource.LUMBER, owner.getResource(Resource.LUMBER)
					+ BattleTownConstants.GENERATION_BUILDER_DEMOLISH_LUMBER);

			building.kill();
		} else if ("farm".equals(action)) {
			if (building != null) {
				owner.sendChat(BattleTownConstants.ACTIONMSG_BUILDING_NOTEMPTY);
				return;
			}

			final int newLumber = this.owner.getResource(Resource.LUMBER) - BattleTownConstants.PURCHASE_FARM_LUMBER;

			if (newLumber < 0) {
				this.owner.sendChat(BattleTownConstants.ACTIONMSG_MORE_RESOURCES);
				return;
			}

			this.owner.setResource(Resource.LUMBER, newLumber);

			tile.changeBuilding((tile2) ->
					new BuildingScaffoldServer(tile2, BuildingFarmServer.class,
							BattleTownConstants.BUILDTICKS_FARM, owner));
		} else if ("barracks".equals(action)) {
			if (building != null) {
				owner.sendChat(BattleTownConstants.ACTIONMSG_BUILDING_NOTEMPTY);
				return;
			}

			final int newLumber = this.owner.getResource(Resource.LUMBER) - BattleTownConstants.PURCHASE_BARRACKS_LUMBER;

			if (newLumber < 0) {
				this.owner.sendChat(BattleTownConstants.ACTIONMSG_MORE_RESOURCES);
				return;
			}

			this.owner.setResource(Resource.LUMBER, newLumber);

			tile.changeBuilding((tile2) ->
					new BuildingScaffoldServer(tile2, BuildingBarracksServer.class,
							BattleTownConstants.BUILDTICKS_BARRACKS, owner));
		} else {
			throw new AssertionError();
		}
	}

	@Override
	protected int[] startBattle() {
		return new int[1];
	}

	@Override
	protected void endBattle(final int[] battleData) {}

	@Override
	public int getAttackAgainst(
			final WorldObject other, final boolean isAttacker, final int[] battleData, final int sufferedDamage) {
		return BattleTownConstants.ATTACK_BUILDER;
	}

	@Override
	public int getDefenseAgainst(
			final WorldObject other, final boolean isAttacker, final int[] battleData, final int sufferedDamage) {
		return BattleTownConstants.WEAPON_TYPE_TERRAIN.equals(other.getWeaponType()) ?
				Integer.MAX_VALUE : BattleTownConstants.DEFENSE_BUILDER;
	}

	@Override
	public int getAttackPriority(final WorldObject other, final boolean isAttacker, final int[] battleData,
			final int sufferedDamage) {
		return battleData[0] < 1 &&
				(isAttacker || this.owner == null || (!this.owner.equals(other.getOwner())) && other.owner != null) ?
				BattleTownConstants.ATTACK_BUILDER : Integer.MIN_VALUE;
	}

	@Override
	public void advanceBattle(
			final WorldObject other, final boolean isAttacker, final int[] battleData, final int sufferedDamage) {
		battleData[0]++;
	}
}
