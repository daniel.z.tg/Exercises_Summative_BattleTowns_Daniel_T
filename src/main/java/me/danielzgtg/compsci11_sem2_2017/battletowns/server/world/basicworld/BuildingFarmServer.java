package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import java.util.Collections;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;

public class BuildingFarmServer extends BuildingServer<Void> {

	public BuildingFarmServer(final TileServer tile, final PlayerServer owner) {
		super(tile, BattleTownConstants.HEALTH_FARM, BattleTownConstants.WEAPON_TYPE_CIVIL, owner);
	}

	@Override
	public void tickAdornment() {
		if (this.owner == null) {
			throw new AssertionError();
		}

		this.owner.setResource(Resource.FOOD,
				this.owner.getResource(Resource.FOOD) + BattleTownConstants.GENERATION_FARM_FOOD);
		this.owner.setResource(Resource.FAITH,
				this.owner.getResource(Resource.FAITH) + BattleTownConstants.GENERATION_FARM_FAITH);
		this.owner.setResource(Resource.LUMBER,
				this.owner.getResource(Resource.LUMBER) + BattleTownConstants.GENERATION_FARM_LUMBER);
	}

	@Override
	public boolean canAdornTile(final TileServer tile) {
		return true;
	}

	@Override
	protected List<String> getAdornmentActions() {
		return Collections.emptyList();
	}

	@Override
	protected void doPerformAction(final String action) {
		throw new AssertionError();
	}

	@Override
	public StringBuilder getAdornmentData() {
		return new StringBuilder();
	}

	@Override
	protected Void startBattle() {
		return null;
	}

	@Override
	protected void endBattle(final Void battleData) {}

	@Override
	public int getAttackAgainst(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return 0;
	}

	@Override
	public int getDefenseAgainst(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return BattleTownConstants.DEFENSE_FARM;
	}

	@Override
	public void advanceBattle(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {}

	@Override
	public int getAttackPriority(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return Integer.MIN_VALUE;
	}
}
