package me.danielzgtg.compsci11_sem2_2017.battletowns.server.test;

import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.GameServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

public class PlayerTest extends PlayerServer {

	private boolean disconnected;
	private String turnName;
	private TileNetwork[][] tiles;
	private int xSize;
	private int ySize;
	private int[] localResources = new int[Resource.RESOURCE_LIST.size()];
	private boolean localReady;

	public PlayerTest(final String name, final GameServer server) {
		super(name, server);
	}

	/*packaged*/ final void chat(final String message) {
		Log.log("[PlayerTest] Sending chat message: " + message);
		this.checkConnection();
		this.handleChat(message);
	}

	/*packaged*/ final void ready() {
		Log.log("[PlayerTest] Will try to make ready");

		this.checkConnection();
		this.makeReady();

		this.localReady = true;
	}

	/*packaged*/ final void pass() {
		Log.log("[PlayerTest] Will try to pass");

		if (this.turnName.length() == 0) {
			Log.log("[PlayerTest] Passing");
			this.checkConnection();
			this.handlePass();
		} else {
			Log.log("[PlayerTest] Refused to pass, not my turn!");
		}
	}

	/*packaged*/ final void requestWorldSize(final int xSize, final int ySize) {
		this.checkConnection();
		this.handleWorldSizeRequest(xSize, ySize);
	}

	/*packaged*/ final void requestWorldType(final String type) {
		this.checkConnection();
		this.handleWorldTypeRequest(type);
	}

	/*packaged*/ final void stop() {
		this.checkConnection();
		this.server.pipe.send(() -> this.server.stop("Console shutdown"), true);
		if (!this.disconnected) {
			throw new AssertionError();
		}
	}

	/*packaged*/ final boolean isClientReady() {
		final boolean localReady = this.localReady;
		if (localReady != this.isReady()) {
			throw new AssertionError("Client/Server Ready out of sync!");
		}

		return localReady;
	}

	/*packaged*/ final int getClientResource(final Resource resource) {
		Validate.notNull(resource);

		final int result = this.localResources[resource.ordinal()];
		Validate.require(this.getResource(resource) == result);

		return result;
	}

	@SuppressWarnings("ConstantConditions")
	/*packaged*/ final TileNetwork getClientTile(final int x, final int y) {
		if (this.tiles == null) {
			throw new IllegalStateException("World does not exist!");
		} else if (x < 0 || y < 0 || x >= this.xSize || y >= this.ySize) {
			throw new ArrayIndexOutOfBoundsException();
		}

		final TileNetwork result = this.tiles[x][y];

		if (result == null) {
			throw new AssertionError("Null Tile!");
		}

		final WorldServer serverWorld = this.server.getWorld();

		if (serverWorld == null) {
			throw new IllegalStateException("Server world does not exist!");
		}

		final TileServer serverTile = serverWorld.getTile(x, y);

		if (serverTile == null) {
			throw new IllegalStateException("Server Tile does not exist!");
		}

		final TileNetwork serverTileRepresentation = serverTile.getRepresentation();

		if (serverTileRepresentation == null) {
			throw new IllegalStateException("");
		}

		if (!serverTileRepresentation.equals(result)) {
			throw new AssertionError("Client/Server Tiles out of sync!");
		}

		return result;
	}

	/*packaged*/ final void performAction(final int x, final int y, final boolean building, final String action) {
		this.checkConnection();

		if (this.tiles == null) {
			Log.log("[PlayerTest] Refused to send action: World does not exist to perform action.");
			return;
		}

		if (x < 0 || y < 0 || x >= this.xSize || y >= this.ySize) {
			Log.log("[PlayerTest] Refused to send action: Position does not exist in world.");
			return;
		}

		final TileNetwork tile = this.tiles[x][y];

		if (tile == null) {
			Log.log("[PlayerTest] Refused to send action: Still waiting for tile replication.");
			return;
		}

		if ((building ? tile.getBuildingData() : tile.getUnitData()).length() == 0) {
			Log.log("[PlayerTest] Refused to send action: Tile adornment does not exist on tile.");
			return;
		}

		this.handleAction(x, y, building, action);
	}

	/*packaged*/ final boolean hasClientWorld() {
		return this.tiles != null;
	}

	/*packaged*/ final int getClientXSize() {
		return this.xSize;
	}

	/*packaged*/ final int getClientYSize() {
		return this.ySize;
	}

	@Override
	protected void doBegin(final int xSize, final int ySize) {
		if (xSize < 0 || ySize < 0) {
			throw new IllegalArgumentException();
		}

		if ((xSize | ySize) == 0) {
			Log.log("[PlayerTest] DoFinish");

			if (this.tiles != null) {
				Log.log("[PlayerTest] ClearTiles");

				for (int x = 0; x < this.xSize; x++) {
					for (int y = 0; y < this.ySize; y++) {
						Log.log("[PlayerTest] FinishTile: " + x + ", " + y + ", "
								+ String.valueOf(this.tiles[x][y]));
					}
				}

				this.tiles = null;
			}

			this.localReady = false;
		} else {
			Log.log("[PlayerTest] DoBegin " + xSize + ", " + ySize);

			this.xSize = xSize;
			this.ySize = ySize;

			this.tiles = new TileNetwork[xSize][ySize];
		}
	}

	@Override
	public void doSendChat(final String message) {
		this.checkConnection();
		Log.log("[PlayerTest] Chat: " + message);
	}

	@Override
	protected void doDisconnect(final String reason) {
		Log.log("[PlayerTest] Disconnect: " + reason);
		this.disconnected = true;
	}

	@Override
	protected void doIndicateTurn(final String playerName) {
		this.checkConnection();
		Validate.notNull(playerName);

		Log.log("[PlayerTest] It is now " + (playerName.length() == 0 ? "my turn" : playerName + "'s turn"));
		this.turnName = playerName;
	}

	@Override
	protected void doSendBattle(final BattleResult battle) {
		this.checkConnection();
		Validate.notNull(battle);

		Log.log("[PlayerTest] Battle Received: " + battle.toString());
	}

	@Override
	protected void doReplySimulation(final BattleResult battle) {
		this.checkConnection();
		Validate.notNull(battle);

		Log.log("[PlayerTest] Simulation Replay: " + battle.toString());
	}

	@Override
	protected void doSendUpdate(final TileNetwork update) {
		this.checkConnection();
		Validate.notNull(update);

		Log.log("[PlayerTest] TileUpdate: " + update.toString());

		if (this.tiles == null) {
			throw new IllegalStateException("World does not exist!");
		}

		final TileNetwork[] subtiles = this.tiles[update.x];
		Log.log("[PlayerTest] TileReplaced: " + String.valueOf(subtiles[update.y]));
		subtiles[update.y] = update;
	}

	@Override
	protected void doSendResource(final Resource resource, final int amount) {
		this.checkConnection();
		Validate.notNull(resource);
		Validate.require(this.getResource(resource) == amount);

		this.localResources[resource.ordinal()] = amount;
	}

	private final void checkConnection() {
		if (this.disconnected) {
			throw new IllegalStateException("Disconnected");
		}
	}

	@Override
	public void run() {

	}
}
