package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;

public class BuildingBarracksServer extends BuildingServer<Void> {

	private static final List<String> ACTIONS = Collections.unmodifiableList(Arrays.asList(
			"builder", "soldier", "priest"
	));

	public BuildingBarracksServer(final TileServer tile, final PlayerServer owner) {
		super(tile, BattleTownConstants.HEALTH_BARRACKS, BattleTownConstants.WEAPON_TYPE_CIVIL, owner);
	}

	@Override
	public void tickAdornment() {
		if (this.owner == null) {
			throw new AssertionError();
		}

		final int newFood = this.owner.getResource(Resource.FOOD) - BattleTownConstants.UPKEEP_BARRACKS_FOOD;

		if (newFood > 0) {
			this.owner.setResource(Resource.FOOD, newFood);
		} else {
			this.owner.sendChat(BattleTownConstants.ACTIONMSG_UPKEEP_UNMET);

			this.kill();
		}
	}

	@Override
	public boolean canAdornTile(final TileServer tile) {
		return true;
	}

	@Override
	protected List<String> getAdornmentActions() {
		return ACTIONS;
	}

	@Override
	protected void doPerformAction(final String action) {
		if (this.owner == null) {
			throw new AssertionError();
		}

		if ("builder".equals(action)) {
			if (this.getTile().getUnit() != null) {
				owner.sendChat(BattleTownConstants.ACTIONMSG_UNIT_OCCUPIED);
				return;
			}

			final int newFood = this.owner.getResource(Resource.FOOD) - BattleTownConstants.PURCHASE_BUILDER_FOOD;

			if (newFood < 0) {
				this.owner.sendChat(BattleTownConstants.ACTIONMSG_MORE_RESOURCES);
				return;
			}

			this.owner.setResource(Resource.FOOD, newFood);

			this.getTile().spawnUnit((tile) -> {
				return new UnitBuilderServer(tile, this.owner);
			});
		} else if ("soldier".equals(action)) {
			if (this.getTile().getUnit() != null) {
				owner.sendChat(BattleTownConstants.ACTIONMSG_UNIT_OCCUPIED);
				return;
			}

			final int newFood = this.owner.getResource(Resource.FOOD) - BattleTownConstants.PURCHASE_SOLDIER_FOOD;

			if (newFood < 0) {
				this.owner.sendChat(BattleTownConstants.ACTIONMSG_MORE_RESOURCES);
				return;
			}

			this.owner.setResource(Resource.FOOD, newFood);

			this.getTile().spawnUnit((tile) -> {
				return new UnitSoldierServer(tile, this.owner);
			});
		} else if ("priest".equals(action)) {
			if (this.getTile().getUnit() != null) {
				owner.sendChat(BattleTownConstants.ACTIONMSG_UNIT_OCCUPIED);
				return;
			}

			final int newFood = this.owner.getResource(Resource.FOOD) - BattleTownConstants.PURCHASE_PRIEST_FOOD;

			if (newFood < 0) {
				this.owner.sendChat(BattleTownConstants.ACTIONMSG_MORE_RESOURCES);
				return;
			}

			this.owner.setResource(Resource.FOOD, newFood);

			this.getTile().spawnUnit((tile) -> {
				return new UnitPriestServer(tile, this.owner);
			});
		} else {
			throw new AssertionError();
		}
	}

	@Override
	public StringBuilder getAdornmentData() {
		return new StringBuilder();
	}

	@Override
	protected Void startBattle() {
		return null;
	}

	@Override
	protected void endBattle(final Void battleData) {}

	@Override
	public int getAttackAgainst(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return 0;
	}

	@Override
	public int getDefenseAgainst(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return BattleTownConstants.DEFENSE_BARRACKS;
	}

	@Override
	public void advanceBattle(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {}

	@Override
	public int getAttackPriority(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return Integer.MIN_VALUE;
	}
}
