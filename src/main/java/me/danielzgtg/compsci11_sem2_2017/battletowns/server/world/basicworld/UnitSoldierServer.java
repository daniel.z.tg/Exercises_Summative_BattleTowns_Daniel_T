package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;

public class UnitSoldierServer extends UnitMilitaryServer<int[]> {

	public static final List<String> ACTIONS = Collections.unmodifiableList(Arrays.asList("pillage"));

	protected UnitSoldierServer(final TileServer tile, final PlayerServer owner) {
		super(tile, BattleTownConstants.HEALTH_SOLDIER, BattleTownConstants.WEAPON_TYPE_SOLDIER, owner);
	}

	@Override
	public void tickAdornment() {
		final int newFood = this.owner.getResource(Resource.FOOD) - BattleTownConstants.UPKEEP_SOLDIER_FOOD;

		if (newFood > 0) {
			this.owner.setResource(Resource.FOOD, newFood);
		} else {
			this.owner.sendChat(BattleTownConstants.ACTIONMSG_UPKEEP_UNMET);

			this.kill();
		}
	}

	@Override
	public boolean canAdornTile(final TileServer tile) {
		return true;
	}

	@Override
	protected List<String> getAdornmentActions() {
		return ACTIONS;
	}

	@Override
	protected void doPerformAction(final String action) {
		if ("pillage".equals(action)) {
			final BuildingServer<?> building = this.getTile().getBuilding();
			if (building != null) {
				WorldObject.battle(this, building, false);
			}
		} else {
			throw new AssertionError();
		}
	}

	@Override
	public StringBuilder getAdornmentData() {
		return new StringBuilder();
	}

	@Override
	protected int[] startBattle() {
		return new int[1];
	}

	@Override
	protected void endBattle(final int[] battleData) {}

	@Override
	public int getAttackAgainst(final WorldObject other, final boolean isAttacker, final int[] battleData,
			final int sufferedDamage) {
		return BattleTownConstants.ATTACK_SOLDIER;
	}

	@Override
	public int getDefenseAgainst(final WorldObject other, final boolean isAttacker, final int[] battleData,
			final int sufferedDamage) {
		return BattleTownConstants.DEFENSE_SOLDIER;
	}

	@Override
	public void advanceBattle(final WorldObject other, final boolean isAttacker, final int[] battleData,
			final int sufferedDamage) {
		battleData[0]++;
	}

	@Override
	public int getAttackPriority(final WorldObject other, final boolean isAttacker, final int[] battleData,
			final int sufferedDamage) {
		return (isAttacker || battleData[0] < 2) ? BattleTownConstants.PRIORITY_SOLDIER : Integer.MIN_VALUE;
	}
}
