package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore;

import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;
import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

@SuppressWarnings("WeakerAccess")
public abstract class TileAdornment<B> extends WorldObject<B> {

	private TileServer<?> tile;

	protected TileAdornment(final TileServer tile,
			final boolean movable, final int initialHealth, final String weaponType, final PlayerServer owner) {
		super(tile.getX(), tile.getY(), movable, tile.getWorld(), initialHealth, weaponType, owner);

		this.tile = tile;
	}

	@Override
	public final void tick() {
		this.tickAdornment();

		if (!this.canAdornTile(this.tile)) {
			this.kill();
		}
	}

	public abstract void tickAdornment();

	public final TileServer<?> getTile() {
		return this.tile;
	}

	/*packaged*/ final void setTile(final TileServer tile) {
		Validate.notNull(tile);

		if (!this.canAdornTile(tile)) {
			throw new IllegalArgumentException();
		}

		this.tile = tile;
		this.copyPosition(tile);
	}

	public abstract boolean canAdornTile(final TileServer tile);

	public final List<String> getActions() {
		final List<String> result = this.getAdornmentActions();

		if (result == null) {
			throw new AssertionError();
		}

		for (final String s : result) {
			if (!StringUtils.containsNonWhitespace(s)) {
				throw new AssertionError();
			}

			if (s.contains(",")) {
				throw new AssertionError();
			}
		}

		return result;
	}

	protected abstract List<String> getAdornmentActions();

	public final boolean isValidAction(final String action) {
		return this.getActions().contains(action);
	}

	public final void performAction(final String action) {
		Validate.notNull(action);

		if (!this.isValidAction(action)) {
			throw new IllegalArgumentException("Invalid action!");
		}

		this.doPerformAction(action);

		//		if (this instanceof UnitServer) {
		//			this.tile.unitModifiedEvent.fire(null);
		//		} else if (this instanceof BuildingServer) {
		//			this.tile.buildingModifiedEvent.fire(null);
		//		} else {
		//			throw new AssertionError();
		//		}
	}

	protected abstract void doPerformAction(final String action);

	@Override
	public final StringBuilder getPartData() {
		final List<String> actions = this.getActions();
		return this.getAdornmentData().append(',').append(StringUtils.joinStrings(actions, ","))
				.append(',').append(actions.size());
	}

	public abstract StringBuilder getAdornmentData();
}
