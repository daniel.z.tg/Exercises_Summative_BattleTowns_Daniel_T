package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;

public final class TileBarrenServer extends TileServer<Void> {

	public TileBarrenServer(final int x, final int y, final WorldServer world) {
		super(x, y, world, Integer.MAX_VALUE, BattleTownConstants.WEAPON_TYPE_TERRAIN);
	}

	@Override
	public final void tileTick() {}

	@Override
	public final TileServer getReplacement() {
		return new TileBarrenServer(this.getX(), this.getY(), this.world);
	}

	@Override
	protected final Void startBattle() {
		return null;
	}

	@Override
	protected final void endBattle(final Void battleData) {}

	@Override
	public final int getAttackAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return 0;
	}

	@Override
	public final int getDefenseAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return Integer.MAX_VALUE;
	}

	@Override
	public final int getAttackPriority(final WorldObject other, final boolean isAttacker,
			final Void battleData, final int sufferedDamage) {
		return Integer.MIN_VALUE;
	}

	@Override
	public void advanceBattle(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {}

	@Override
	public final StringBuilder getPartData() {
		return new StringBuilder();
	}
}
