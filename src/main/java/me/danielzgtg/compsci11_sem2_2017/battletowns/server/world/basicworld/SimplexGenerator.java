package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldGenerator;

public final class SimplexGenerator implements WorldGenerator {

	public static final SimplexGenerator INSTANCE = new SimplexGenerator();

	private SimplexGenerator() {
		if (INSTANCE != null) {
			throw new IllegalStateException();
		}
	}

	@Override
	public TileServer generateTile(final int x, final int y, final WorldServer world) {
		if (world.getXSize() < 4 || world.getYSize() < 4) {
			throw new IllegalArgumentException();
		}

		if ((x & 1) == 0) {
			if ((y & 1) == 0) {
				return new TileSpawnServer(x, y, world);
			} else {
				return new TileSacredServer(x, y, world);
			}
		} else {
			return new TileBarrenServer(x, y, world);
		}
	}
}
