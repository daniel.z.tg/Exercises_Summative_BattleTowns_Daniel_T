package me.danielzgtg.compsci11_sem2_2017.battletowns.server;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.NetworkProtocol;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.io.IOStreamUtil;

import static me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants.DEBUG;

@SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
public final class NetworkPlayerServer extends PlayerServer {

	private final InputStream in;
	private final OutputStream out;
	private final Thread thread;
	private volatile boolean disconnected;
	private volatile String disconnectReason;
	private final List<Runnable> work = new LinkedList<>();

	public NetworkPlayerServer(final String name, final GameServer server,
			final InputStream in, final OutputStream out) {
		super(name, server);

		Validate.notNull(in);
		Validate.notNull(out);

		this.in = in;
		this.out = out;
		this.thread = new Thread(this::playerLogic);
	}

	@Override
	protected void doBegin(final int xSize, final int ySize) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_SERVER_BEGIN);
					IOStreamUtil.writeInt(this.out, xSize);
					IOStreamUtil.writeInt(this.out, ySize);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	public void doSendChat(final String message) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_SERVER_CHAT);
					IOStreamUtil.writeString(this.out, message);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doDisconnect(final String reason) {
		if (!this.disconnected) {
			this.disconnectReason = reason;
			this.disconnected = true;
		}
	}

	@Override
	protected void doIndicateTurn(final String playerName) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_SERVER_TURN);
					IOStreamUtil.writeString(this.out, playerName);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doSendUpdate(final TileNetwork update) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_SERVER_UPDATE);
					update.serialize(this.out);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doSendBattle(final BattleResult battle) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_SERVER_BATTLE);
					battle.serialize(this.out);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doReplySimulation(final BattleResult battle) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_SERVER_SIMULATION);
					battle.serialize(this.out);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doSendResource(final Resource resource, final int amount) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_SERVER_RESOURCE);
					this.out.write(resource.ordinal());
					IOStreamUtil.writeInt(this.out, amount);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	public final void run() {
		this.thread.start();
	}

	private final void playerLogic() {
		this.server.program.addThread(Thread.currentThread(), "Server Network");

		try {
			try {
				if (DEBUG) {
					Log.log("[NetworkPlayerServer] Connected: " + this.toString());
				}

				long slept = 0;
				final List<Runnable> globalWork = this.work;
				final List<Runnable> tmpWork = new LinkedList<>();
				while (!this.disconnected) {
					if (this.in.available() != 0) {
						final byte id = (byte) this.in.read();

						this.handlePacket(id);
						slept = 0;
					}

					synchronized(globalWork) {
						tmpWork.addAll(globalWork);
						globalWork.clear();
					}

					for (final Iterator<Runnable> iter = tmpWork.iterator(); iter.hasNext();) {
						final Runnable job = iter.next();

						job.run();

						iter.remove();
					}

					if (tmpWork.size() != 0) {
						try {
							slept += NetworkProtocol.SLEEP_TIME;
							Thread.sleep(NetworkProtocol.SLEEP_TIME);
						} catch (final Exception ignore) {}

						if (slept > NetworkProtocol.KEEPALIVE_TIME) {
							this.out.write(NetworkProtocol.PACKETID_GLOBAL_KEEPALIVE);
							slept = 0;
						}
					}
				}
			} catch (final Exception e) {
				Log.log("[NetworkPlayerServer] Connection failed: " + this.toString());
				this.disconnected = true;
				this.server.kickPlayer(this, e.getMessage());
				return;
			}

			try {
				this.out.write(NetworkProtocol.PACKETID_GLOBAL_DISCONNECT);
				IOStreamUtil.writeString(this.out, this.disconnectReason);
			} catch (final Exception e) {
				Log.log("[NetworkPlayerServer] IO Error while disconnecting: " + this.toString());
			}
		} finally {
			if (DEBUG) {
				Log.log("[NetworkPlayerServer] Disconnecting: " + this.toString());
			}

			try {
				this.out.close();
				this.in.close();
			} catch (final Exception e) {
				Log.log("[NetworkPlayerServer] IO Error while closing: " + this.toString());
			}
		}
	}

	private final void handlePacket(final byte id) throws IOException {
		switch (id) {
			case 0:
				return;
			case 1:
				this.server.kickPlayer(this, BattleTownConstants.KICKREASON_SELF);
				return;
			case NetworkProtocol.PACKETID_CLIENT_CHAT:
				this.handleChat(IOStreamUtil.readString(this.in));
				return;
			case NetworkProtocol.PACKETID_CLIENT_READY:
				this.makeReady();
				return;
			case NetworkProtocol.PACKETID_CLIENT_SIZEREQUEST:
				this.handleWorldSizeRequest(IOStreamUtil.readInt(this.in), IOStreamUtil.readInt(this.in));
				return;
			case NetworkProtocol.PACKETID_CLIENT_TYPEREQUEST:
				this.handleWorldTypeRequest(IOStreamUtil.readString(this.in));
				return;
			case NetworkProtocol.PACKETID_CLIENT_ACTION:
				this.handleAction(IOStreamUtil.readInt(this.in), IOStreamUtil.readInt(this.in),
						this.in.read() != 0, IOStreamUtil.readString(this.in));
				return;
			case NetworkProtocol.PACKETID_CLIENT_BATTLE:
				this.handleBattle(this.in.read() != 0,
						IOStreamUtil.readInt(this.in), IOStreamUtil.readInt(this.in),
						IOStreamUtil.readInt(this.in), IOStreamUtil.readInt(this.in),
						this.in.read() != 0);
				return;
			case NetworkProtocol.PACKETID_CLIENT_PASS:
				this.handlePass();
				return;
			default:
				throw new IllegalArgumentException("Invalid Packet id");
		}
	}
}
