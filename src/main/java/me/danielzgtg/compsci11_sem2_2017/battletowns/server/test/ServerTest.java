package me.danielzgtg.compsci11_sem2_2017.battletowns.server.test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.GameServer;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.program.Program;
import me.danielzgtg.compsci11_sem2_2017.common.ui.CommandDrivenCLI;
import me.danielzgtg.compsci11_sem2_2017.common.ui.PrintMessenger;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Prompter;

public final class ServerTest extends AppContainer implements Runnable {

	private final Program program = new Program("BattleTownsServerTest");
	private final GameServer server = new GameServer(true, this.program);
	private final PlayerTest player = new PlayerTest("player", this.server);

	private static final Map<String, BiFunction<CommandDrivenCLI<
			Pair<GameServer, PlayerTest>>, String[], Pair<GameServer, PlayerTest>>> COMMANDS;

	static {
		final Map<String, BiFunction<CommandDrivenCLI<
				Pair<GameServer, PlayerTest>>, String[], Pair<GameServer, PlayerTest>>> tmpCommands = new HashMap<>();

		tmpCommands.put("stop", (state, args) -> {
			state.getState().getRight().stop();

			return null;
		});

		tmpCommands.put("ready", (state, args) -> {
			final Pair<GameServer, PlayerTest> system = state.getState();

			state.getMessenger().println("Ready: " + system.getRight().isClientReady());

			return system;
		});

		tmpCommands.put("setready", (state, args) -> {
			final Pair<GameServer, PlayerTest> system = state.getState();

			system.getRight().ready();

			return system;
		});

		tmpCommands.put("pass", (state, args) -> {
			final Pair<GameServer, PlayerTest> system = state.getState();

			system.getRight().pass();

			return system;
		});

		tmpCommands.put("say", (state, args) -> {
			final Pair<GameServer, PlayerTest> system = state.getState();

			if (args.length > 1) {
				final StringBuilder message = new StringBuilder();

				for (int i = 1;;) {
					message.append(args[i]);

					if (++i >= args.length) {
						break;
					} else {
						message.append(' ');
					}
				}

				system.getRight().chat(message.toString());
			} else {
				state.getMessenger().println("Need something to say");
			}

			return system;
		});

		tmpCommands.put("worldsize", (state, args) -> {
			final Pair<GameServer, PlayerTest> system = state.getState();

			if (args.length != 3) {
				state.getMessenger().println("Usage: worldsize xSize ySize");
				return system;
			}

			final int xSize, ySize;

			try {
				xSize = Integer.parseInt(args[1]);
				ySize = Integer.parseInt(args[2]);
			} catch (final NumberFormatException nfe) {
				state.getMessenger().print("Sizes must be integer");
				return system;
			}

			system.getRight().requestWorldSize(xSize, ySize);

			return system;
		});

		tmpCommands.put("worldtype", (state, args) -> {
			final Pair<GameServer, PlayerTest> system = state.getState();

			if (args.length < 2) {
				state.getMessenger().println("Usage: worldtype type");
				return system;
			}

			final StringBuilder type = new StringBuilder();

			for (int i = 1;;) {
				type.append(args[i]);

				if (++i >= args.length) {
					break;
				} else {
					type.append(' ');
				}
			}

			system.getRight().requestWorldType(type.toString());

			return system;
		});

		tmpCommands.put("resources", (state, args) -> {
			final Pair<GameServer, PlayerTest> system = state.getState();
			final PrintMessenger messenger = state.getMessenger();
			final PlayerTest playerTest = system.getRight();

			for (final Resource resource : Resource.RESOURCE_LIST) {
				messenger.formatln("Resource %s at %d", resource.toString(), playerTest.getClientResource(resource));
			}

			return system;
		});

		tmpCommands.put("tiles", (state, args) -> {
			final Pair<GameServer, PlayerTest> system = state.getState();
			final PrintMessenger messenger = state.getMessenger();
			final PlayerTest playerTest = system.getRight();

			if (playerTest.hasClientWorld()) {
				final int xSize = playerTest.getClientXSize();
				for (int x = 0; x < xSize; x++) {
					final int clientYSize = playerTest.getClientYSize();
					for (int y = 0; y < clientYSize; y++) {
						messenger.formatln("Tile at (%d,%d) is %s", x, y, String.valueOf(playerTest.getClientTile(x, y)));
					}
				}
			} else {
				messenger.println("No Client world!");
			}

			return system;
		});

		tmpCommands.put("action_u", (state, args) -> helpAction(state, args, false));

		tmpCommands.put("action_b", (state, args) -> helpAction(state, args, true));

		COMMANDS = Collections.unmodifiableMap(tmpCommands);
	}

	private static Pair<GameServer, PlayerTest> helpAction(final CommandDrivenCLI<Pair<GameServer, PlayerTest>> state,
			final String[] args, final boolean building) {
		final Pair<GameServer, PlayerTest> system = state.getState();
		final PrintMessenger messenger = state.getMessenger();

		if (args.length < 4) {
			messenger.println(building ? "Usage: action_b x y action" : "Usage: action_u x y action");
		}

		final int x, y;

		try {
			x = Integer.parseInt(args[1]);
			y = Integer.parseInt(args[2]);
		} catch (final NumberFormatException nfe) {
			state.getMessenger().print("Positions must be integer");
			return system;
		}

		final StringBuilder action = new StringBuilder();

		for (int i = 3;;) {
			action.append(args[i]);

			if (++i >= args.length) {
				break;
			} else {
				action.append(' ');
			}
		}

		system.getRight().performAction(x, y, building, action.toString());

		return system;
	}

	@Override
	protected void doLaunch() {
		new Thread(this).start();
	}

	@Override
	public void run() {
		this.program.addThread(Thread.currentThread(), "TestMain");

		this.server.launch();
		this.server.loginPlayer(this.player);
		final PrintMessenger messenger = new PrintMessenger(System.out);
		new CommandDrivenCLI<>(ResourceUtils.COMMON_CONSTANT_DATA, null, null,
				COMMANDS, new Pair<>(this.server, this.player), true, null, null,
				null, null, null, null,
				null, new Prompter(ResourceUtils.COMMON_CONSTANT_DATA, System.in, messenger), messenger
		).run();
	}

	public static void main(final String[] args) {
		new ServerTest().launch();
	}
}
