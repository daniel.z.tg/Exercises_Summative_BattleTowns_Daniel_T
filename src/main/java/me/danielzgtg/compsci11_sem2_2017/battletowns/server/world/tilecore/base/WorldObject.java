package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base;

import java.util.LinkedList;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileLayer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.UnitServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.util.Locatable;
import me.danielzgtg.compsci11_sem2_2017.common.Tickable;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Event;

public abstract class WorldObject<B> implements Tickable, Locatable, CompartmentalizedData {

	private int x;
	private int y;
	private final boolean movable;
	private final String weaponType;
	private int health;
	public final int maxHealth;
	private boolean dead = false;

	public final Event<Pair<Integer, Integer>> positionChangedEvent = new Event<>();
	public final Event<Integer> healthChangedEvent = new Event<>();
	public final Event<Void> deathEvent = new Event<>();

	public final PlayerServer owner;
	public final WorldServer world;

	protected WorldObject(final int x, final int y, final boolean movable, final WorldServer world,
			final int maxHealth, final String weaponType, final PlayerServer owner) {
		if (x < 0 || y < 0 || maxHealth <= 0 || weaponType == null || world == null) {
			throw new IllegalArgumentException();
		}

		this.x = x;
		this.y = y;
		this.movable = movable;
		this.world = world;

		this.health = maxHealth;
		this.maxHealth = maxHealth;
		this.weaponType = weaponType;

		this.owner = owner;
	}

	public final PlayerServer getOwner() {
		return this.owner;
	}

	public final boolean isMovable() {
		return this.movable;
	}

	public final void checkMovable() {
		if (!this.movable) {
			throw new UnsupportedOperationException();
		}
	}

	@SuppressWarnings("SuspiciousNameCombination")
	protected final void setPosition(final int x, final int y) {
		this.checkMovable();

		this.x = x;
		this.y = y;

		this.positionChangedEvent.fire(new Pair<>(x, y));
	}

	protected final void copyPosition(final WorldObject other) {
		Validate.notNull(other);
		//this.checkMovable();

		this.setPosition(other.x, other.y);
	}

	@SuppressWarnings("SuspiciousNameCombination")
	protected final void setX(final int x) {
		this.checkMovable();

		this.x = x;

		this.positionChangedEvent.fire(new Pair<>(x, this.y));
	}

	@SuppressWarnings("SuspiciousNameCombination")
	protected final void setY(final int y) {
		this.checkMovable();

		this.y = y;

		this.positionChangedEvent.fire(new Pair<>(this.x, y));
	}

	public final int getX() {
		return this.x;
	}

	public final int getY() {
		return this.y;
	}

	public final WorldServer getWorld() {
		return this.world;
	}

	public final String getWeaponType() {
		return this.weaponType;
	}

	protected abstract B startBattle();

	protected abstract void endBattle(final B battleData);

	public abstract int getAttackAgainst(
			final WorldObject other, final boolean isAttacker, final B battleData, final int sufferedDamage);

	public abstract int getDefenseAgainst(
			final WorldObject other, final boolean isAttacker, final B battleData, final int sufferedDamage);

	public abstract void advanceBattle(
			final WorldObject other, final boolean isAttacker, final B battleData, final int sufferedDamage);

	// Integer.MIN_VALUE requests that the battle be ended
	public abstract int getAttackPriority(
			final WorldObject other, final boolean isAttacker, final B battleData, final int sufferedDamage);

	public final void kill() {
		if (!this.dead) {
			this.setHealth(Integer.MIN_VALUE);
		}
	}

	public final int getHealth() {
		return this.health;
	}


	public final void setHealth(final int newHealth) {
		if (this.dead) {
			throw new IllegalStateException("Already dead!");
		}

		final int maxHealth = this.maxHealth;
		final int finalHealth = newHealth > maxHealth ? maxHealth : newHealth;

		if (this.health != finalHealth) {
			this.health = finalHealth;
			this.healthChangedEvent.fire(finalHealth);
		}

		if (finalHealth <= 0) {
			this.dead = true;
			this.deathEvent.fire(null);
		}
	}

	public final int getMaxHealth() {
		return this.maxHealth;
	}

	public final boolean isDead() {
		return this.dead;
	}

	public static final <A, V> BattleResult battle(final WorldObject<A> attacker,
			final WorldObject<V> victim, final boolean simulation) {
		if (attacker == null || victim ==  null) {
			throw new IllegalArgumentException();
		}

		final int attackerHealth = attacker.getHealth();
		final int victimHealth = victim.getHealth();

		if ((attackerHealth | victimHealth) < 0) {
			throw new AssertionError("Participants are already dead!");
		}

		final A battleDataAttacker = attacker.startBattle();
		final V battleDataVictim = victim.startBattle();

		final List<Integer> battleHistory = new LinkedList<>();
		int attackerSufferedDamage = 0;
		int victimSufferedDamage = 0;
		boolean attackerJustAttacked = false;
		while (true) {
			final int attackerPriority =
					attacker.getAttackPriority(victim, true, battleDataAttacker, attackerSufferedDamage);
			final int victimPriority =
					victim.getAttackPriority(attacker, false, battleDataVictim, victimSufferedDamage);

			if (attackerPriority == Integer.MIN_VALUE && victimPriority == Integer.MIN_VALUE) {
				break;
			}

			final boolean victimAttacks = victimPriority > attackerPriority ||
					victimPriority == attackerPriority && attackerJustAttacked;
			attackerJustAttacked = !victimAttacks;

			if (victimAttacks) {
				final int damage = victim.getAttackAgainst(attacker, false, battleDataVictim, victimSufferedDamage) -
						attacker.getDefenseAgainst(victim, true, battleDataAttacker, attackerSufferedDamage);

				if (damage > 0) {
					attackerSufferedDamage += damage;
					battleHistory.add(-damage - 1);
				} else {
					battleHistory.add(-1);
				}

				if (attackerHealth <= attackerSufferedDamage) {
					break;
				}
			} else {
				final int damage = attacker.getAttackAgainst(victim, true, battleDataAttacker, victimSufferedDamage) -
						victim.getDefenseAgainst(attacker, false, battleDataVictim, attackerSufferedDamage);

				if (damage > 0) {
					victimSufferedDamage += damage;
					battleHistory.add(damage);
				} else {
					battleHistory.add(0);
				}

				if (victimHealth <= victimSufferedDamage) {
					break;
				}
			}

			attacker.advanceBattle(victim, true, battleDataAttacker, attackerSufferedDamage);
			victim.advanceBattle(attacker, false, battleDataVictim, victimSufferedDamage);
		}

		attacker.endBattle(battleDataAttacker);
		victim.endBattle(battleDataVictim);

		final BattleResult result = new BattleResult(attacker, victim, battleHistory);

		if (!simulation) {
			attacker.setHealth(attackerHealth - attackerSufferedDamage);
			victim.setHealth(victimHealth - victimSufferedDamage);

			final WorldServer world = attacker.getWorld();
			if (world != victim.getWorld()) {
				throw new IllegalArgumentException("Inconsistent worlds!");
			}

			if (battleHistory.size() > 0) {
				world.battleEvent.fire(result);
			}
		}

		return result;
	}

	@Override
	public final String internalGetPartData() {
		return this.getPartData().append(',').append(this.getClass().getName())
				.append(',').append(this.health).append(',').append(this.owner == null ? "system" : this.owner.name)
				.append(',').append(this.maxHealth)
				.toString();
	}

	public final TileLayer getLayer() {
		if (this instanceof TileServer) {
			return TileLayer.TILE;
		} else if (this instanceof UnitServer) {
			return TileLayer.UNIT;
		} else if (this instanceof BuildingServer) {
			return TileLayer.BUILDING;
		} else {
			throw new AssertionError();
		}
	}
}
