package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore;

import java.util.function.Consumer;
import java.util.function.Function;

import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Event;

public abstract class TileServer<B> extends WorldObject<B> {

	private BuildingServer<?> building;
	private UnitServer unit;

	private TileServer immediateReplacement;
	private Consumer unitLeaveListener;

	public final Event<BuildingServer> buildingChangedEvent = new Event<>();
	public final Event<Void> buildingModifiedEvent = new Event<>();
	public final Event<UnitServer> unitChangedEvent = new Event<>();
	public final Event<Void> unitModifiedEvent = new Event<>();

	protected TileServer(final int x, final int y, final WorldServer world,
			final int initialHealth, final String weaponType) {
		super(x, y, false, world, initialHealth, weaponType, null);

		this.deathEvent.addListener((ignore) -> {
			final TileServer replacement = this.doGetReplacement();

			if (replacement == null) {
				throw new AssertionError();
			}

			if (this.unit != null) {
				this.unit.kill();
			}

			if (this.building != null) {
				this.building.kill();
			}

			this.getWorld().replaceTile(this.getX(), this.getY(), replacement);
		});
	}

	@Override
	public final void tick() {
		this.tileTick();

		BuildingServer<?> building = this.building;
		if (building != null) {
			building.tick();
		}
		building = this.building;

		UnitServer<?> unit = this.unit;
		if (unit != null) {
			unit.tick();
		}
		unit = this.unit;

		if (building != null) {
			WorldObject.battle(this, building, false);
		}

		if (this.isDead()) {
			return;
		}

		if (unit != null) {
			WorldObject.battle(this, unit, false);

			if (this.isDead()) {
				return;
			}

			if (building != null) {
				WorldObject.battle(building, unit, false);
			}
		}
	}

	public abstract void tileTick();

	public final void replace(final TileServer newTile) {
		this.immediateReplacement = newTile;

		this.kill();
	}

	private final TileServer doGetReplacement() {
		final TileServer immediateReplacement = this.immediateReplacement;

		return immediateReplacement != null ? immediateReplacement : this.getReplacement();
	}

	public abstract TileServer getReplacement();

	public final TileNetwork getRepresentation() {
		return new TileNetwork(this.getX(), this.getY(),
				this.internalGetPartData(),
				this.building == null ? "" : this.building.internalGetPartData(),
				this.unit == null ? "" : this.unit.internalGetPartData());
	}

	public final void destroyBuilding() {
		if (this.building != null) {
			this.building.kill();

			if (this.building != null) {
				throw new AssertionError();
			}
		}
	}

	public final void changeBuilding(final Function<TileServer, BuildingServer> buildingGenerator) {
		Validate.notNull(buildingGenerator);

		this.destroyBuilding();

		final BuildingServer<?> newBuilding = buildingGenerator.apply(this);

		Validate.notNull(newBuilding);
		if (newBuilding.getTile() != this) {
			throw new AssertionError();
		}

		newBuilding.deathEvent.addListener((ignore) -> {
			this.building = null;
			this.buildingChangedEvent.fire(null);
		});

		newBuilding.healthChangedEvent.addListener((ignore) -> {
			this.buildingChangedEvent.fire(null);
		});

		this.building = newBuilding;
		this.buildingChangedEvent.fire(newBuilding);
	}

	public final void destroyUnit() {
		if (this.unit != null) {
			this.unit.kill();

			if (this.unit != null || this.unitLeaveListener != null) {
				throw new AssertionError();
			}
		}
	}

	public final void spawnUnit(final Function<TileServer, UnitServer> unitGenerator) {
		Validate.notNull(unitGenerator);

		this.destroyUnit();

		final UnitServer<?> newUnit = unitGenerator.apply(this);

		Validate.notNull(newUnit);
		if (this.unit != null || this.unitLeaveListener != null) {
			throw new AssertionError();
		}

		this.doTakeUnit(newUnit);
	}

	public final void takeUnit(final UnitServer unit) {
		Validate.notNull(unit);

		this.destroyUnit();

		unit.setTile(this);
		this.doTakeUnit(unit);
	}

	@SuppressWarnings("unchecked")
	private final void doTakeUnit(final UnitServer<?> unit) {
		Validate.notNull(unit);

		if (this.unit != null || this.unitLeaveListener != null) {
			throw new AssertionError();
		}

		final Consumer<Integer> unitHealthListener = integer -> this.unitChangedEvent.fire(null);

		final Consumer unitLeaveListener = new Consumer() {
			@Override
			public void accept(final Object o) {
				final TileServer _this = TileServer.this;

				if (_this.unitLeaveListener != this) {
					throw new AssertionError();
				}

				unit.deathEvent.removeListener(_this.unitLeaveListener);
				unit.positionChangedEvent.removeListener(_this.unitLeaveListener);
				unit.healthChangedEvent.removeListener(unitHealthListener);

				_this.unit = null;
				_this.unitChangedEvent.fire(null);
				_this.unitLeaveListener = null;
			}
		};

		this.unitLeaveListener = unitLeaveListener;
		unit.deathEvent.addListener(unitLeaveListener);
		unit.positionChangedEvent.addListener(unitLeaveListener);

		unit.healthChangedEvent.addListener(unitHealthListener);

		this.unit = unit;
		this.unitChangedEvent.fire(unit);
	}

	public final BuildingServer getBuilding() {
		return this.building;
	}

	public final UnitServer getUnit() {
		return this.unit;
	}
}
