package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import java.util.Collections;
import java.util.List;

import java.lang.reflect.Constructor;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

public final class BuildingScaffoldServer extends BuildingServer<Void> {

	private final int buildTicksTotal;
	private int ticksLeft;

	private final Class<? extends BuildingServer> completionBuildingClass;
	private final Constructor<? extends BuildingServer> completionBuildingConstructor;

	public BuildingScaffoldServer(final TileServer tile,
			final Class<? extends BuildingServer> completionBuildingClass,
			final int buildTicks, final PlayerServer owner) {
		super(tile, BattleTownConstants.HEALTH_BUILDINGSCAFFOLD,
				BattleTownConstants.WEAPON_TYPE_BUILDINGSCAFFOLD, owner);

		Validate.notNull(completionBuildingClass);

		try {
			this.completionBuildingConstructor =
					completionBuildingClass.getConstructor(TileServer.class, PlayerServer.class);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}

		this.completionBuildingClass = completionBuildingClass;
		this.ticksLeft = buildTicks;
		this.buildTicksTotal = buildTicks;
	}

	@Override
	public final void tickAdornment() {
		final int newLumber = this.owner.getResource(Resource.LUMBER) - BattleTownConstants.UPKEEP_SCAFFOLD_LUMBER;

		if (newLumber > 0) {
			this.owner.setResource(Resource.LUMBER, newLumber);
		} else {
			this.owner.sendChat(BattleTownConstants.ACTIONMSG_UPKEEP_UNMET);

			this.kill();
			return;
		}

		if (--this.ticksLeft == 0) {
			this.finishBuilding();
		} else {
			this.getTile().buildingModifiedEvent.fire(null);
		}
	}

	private void finishBuilding() {
		final TileServer tile = this.getTile();
		final BuildingServer newBuilding;

		try {
			newBuilding = this.completionBuildingConstructor.newInstance(tile, this.owner);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}

		this.getTile().changeBuilding(argTile -> {
			if (argTile != tile) {
				throw new AssertionError();
			}

			return newBuilding;
		});
	}

	@Override
	protected final Void startBattle() {
		return null;
	}

	@Override
	protected final void endBattle(final Void battleData) {}

	@Override
	public final int getAttackAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return 0;
	}

	@Override
	public final int getDefenseAgainst(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {
		return BattleTownConstants.DEFENSE_BUILDINGSCAFFOLD;
	}

	@Override
	public final int getAttackPriority(final WorldObject other, final boolean isAttacker, final Void battleData,
			final int sufferedDamage) {
		return Integer.MIN_VALUE;
	}

	@Override
	public void advanceBattle(
			final WorldObject other, final boolean isAttacker, final Void battleData, final int sufferedDamage) {}

	@Override
	public final StringBuilder getAdornmentData() {
		return new StringBuilder().append(this.ticksLeft).append(',').append(this.buildTicksTotal)
				.append(',').append(this.completionBuildingClass.getName());
	}

	@Override
	public final boolean canAdornTile(final TileServer tile) {
		return true;
	}

	@Override
	public List<String> getAdornmentActions() {
		return Collections.singletonList("demolish");
	}

	@Override
	protected void doPerformAction(final String action) {
		if ("demolish".equals(action)) {
			this.kill();
		} else {
			throw new AssertionError();
		}
	}
}
