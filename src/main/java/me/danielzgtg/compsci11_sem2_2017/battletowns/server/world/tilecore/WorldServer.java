package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore;

import java.util.function.Consumer;

import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.WorldCommon;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.GameServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.UnitBuilderServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldGenerator;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Event;

import static me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants.DEBUG;

@SuppressWarnings("WeakerAccess")
public final class WorldServer extends WorldCommon {

	private /*mut*/ final TileServer[][] tiles;

	public final Event<BattleResult> battleEvent = new Event<>();

	public final GameServer server;

	public final GameServer getServer() {
		return this.server;
	}

	@SuppressWarnings("unchecked")
	public WorldServer(final int xSize, final int ySize, final WorldGenerator generator, final GameServer server) {
		super(xSize, ySize);

		Validate.notNull(generator);
		Validate.notNull(server);

		this.server = server;

		this.tiles = new TileServer[xSize][ySize];
		for (int x = 0; x < this.xSize; x++) {
			final TileServer[] subtiles = this.tiles[x];

			for (int y = 0; y < this.ySize; y++) {
				final TileServer<?> tile = generator.generateTile(x, y, this);

				if (tile == null) {
					throw new IllegalArgumentException(String.format("Bad generator at %d,%d", x, y));
				}

				if (DEBUG) {
					Log.log("[BattleTownsWorldServer] GenerateTile: " + x + ", " + y + ", " + tile);
				}

				subtiles[y] = tile;

				final Consumer listener = (ignore) -> {
					if (!tile.isDead()) {
						server.updateTile(tile.getRepresentation());
					} else {
						Log.log("[BattleTownsWorldServer] Updating a dead tile?: " + String.valueOf(tile));
					}
				};

				tile.buildingModifiedEvent.addListener(listener);
				tile.buildingChangedEvent.addListener(listener);
				tile.unitModifiedEvent.addListener(listener);
				tile.unitChangedEvent.addListener(listener);
			}
		}
	}

	public final boolean isValidTile(final int x, final int y) {
		return x >= 0 && y >= 0 && x < this.xSize && y < this.ySize;
	}

	public final TileServer getTile(final int x, final int y) {
		if (!this.isValidTile(x, y)) {
			throw new ArrayIndexOutOfBoundsException();
		}

		return this.tiles[x][y];
	}

	public final void performInitialReplication(final PlayerServer player) {
		Validate.notNull(player);

		if (DEBUG) {
			Log.log("[BattleTownsWorldServer] Initial Replication for: " + player.name);
		}

		final int xSize = this.xSize;
		for (int x = 0; x < xSize; x++) {
			final int ySize = this.ySize;
			for (int y = 0; y < ySize; y++) {
				player.sendUpdate(this.tiles[x][y].getRepresentation());
			}
		}
	}

	@Override
	public final void tick() {
		if (DEBUG) {
			Log.log("[BattleTownsWorldServer] WorldTick");
		}

		for (int x = 0; x < this.xSize; x++) {
			final TileServer[] subtiles = this.tiles[x];

			for (int y = 0; y < this.ySize; y++) {
				subtiles[y].tick();
			}
		}
	}

	/*packaged*/ final void replaceTile(final int x, final int y, final TileServer newTile) {
		if (x < 0 || x > this.xSize || y < 0 || y > this.ySize || newTile == null ||
				newTile.getX() != x || newTile.getY() != y || newTile.isDead()) {
			throw new IllegalArgumentException();
		}

		if (DEBUG) {
			Log.log("[BattleTownsWorldServer] ReplaceTile: " + x + ", " + y + ", " + newTile);
		}

		final TileServer[] subtiles = this.tiles[x];
		final TileServer lastTile = subtiles[y];
		if (!(lastTile == null || lastTile.isDead())) {
			throw new IllegalStateException("Last tile is still there");
		}

		this.tiles[x][y] = newTile;
	}

	public final void helpRemovePlayer(final PlayerServer player) {
		Validate.notNull(player);

		if (DEBUG) {
			Log.log("[BattleTownsWorldServer] HelpRemovePlayer: " + player.name);
		}

		for (int x = 0; x < this.xSize; x++) {
			final TileServer[] subtiles = this.tiles[x];

			for (int y = 0; y < this.ySize; y++) {
				final TileServer tile = subtiles[y];

				final BuildingServer building = tile.getBuilding();
				if (building != null && building.owner == player) {
					if (DEBUG) {
						Log.log("[BattleTownsWorldServer] HelpRemovePlayerBuilding: "
								+ x + ", " + y + ", " + player.name);
					}

					building.kill();
				}

				final UnitServer unit = tile.getUnit();
				if (unit != null && unit.owner == player) {
					if (building != null && building.owner == player) {
						if (DEBUG) {
							Log.log("[BattleTownsWorldServer] HelpRemovePlayerUnit: "
									+ x + ", " + y + ", " + player.name);
						}

						unit.kill();
					}
				}
			}
		}
	}

	public final boolean spawnPlayer(final PlayerServer player) {
		Validate.notNull(player);

		if (DEBUG) {
			Log.log("[BattleTownsWorldServer] SpawnPlayer: " + player.name);
		}

		this.helpRemovePlayer(player);

		for (int x = 0; x < this.xSize; x++) {
			final TileServer[] subtiles = this.tiles[x];

			for (int y = 0; y < this.ySize; y++) {
				final TileServer<?> tile = subtiles[y];

				if (tile.getBuilding() == (Object) tile.getUnit()) { // (building | unit) == null
					if (DEBUG) {
						Log.log("[BattleTownsWorldServer] SpawnPlayerAt: " + x + ", " + y + ", " + player.name);
					}

					tile.spawnUnit((tile2) -> {
						if (tile2 != tile) {
							throw new AssertionError();
						}

						return new UnitBuilderServer(tile2, player);
					});

					return true;
				}
			}
		}

		return false;
	}
}
