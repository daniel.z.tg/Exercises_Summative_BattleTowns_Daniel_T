package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base;

public abstract interface CompartmentalizedData {

	public abstract String internalGetPartData();

	public abstract StringBuilder getPartData();
}
