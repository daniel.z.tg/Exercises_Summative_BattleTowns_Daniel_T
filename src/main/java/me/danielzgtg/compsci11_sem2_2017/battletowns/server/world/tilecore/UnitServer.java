package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore;

import java.util.Collections;

import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

public abstract class UnitServer<B> extends TileAdornment<B> {

	protected UnitServer(final TileServer tile,
			final int initialHealth, final String weaponType, final PlayerServer owner) {
		super(tile, true, initialHealth, weaponType, owner);
	}

	public final BattleResult fight(final TileServer tile, final boolean simulation, final boolean move) {
		Validate.notNull(tile);

		final UnitServer<?> otherUnit = tile.getUnit();

		if (!this.canAdornTile(tile)) {
			return null;
		}

		if (otherUnit == null) {
			if (!simulation && move) {
				tile.takeUnit(this);
			}

			return null;
		} else {
			final BattleResult result = WorldObject.battle(this, otherUnit, simulation);

			if (!simulation && otherUnit.isDead() && move) {
				tile.takeUnit(this);
			}

			return result;
		}
	}
}
