package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld;

import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.UnitServer;

public abstract class UnitMilitaryServer<B> extends UnitServer<B> {

	protected UnitMilitaryServer(final TileServer tile, final int initialHealth, final String weaponType,
			final PlayerServer owner) {
		super(tile, initialHealth, weaponType, owner);
	}
}
