package me.danielzgtg.compsci11_sem2_2017.battletowns.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import java.io.FileOutputStream;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.WorldCommon;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.SimplexGenerator;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileAdornment;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.UnitServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldGenerator;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.BlockingPipe;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.program.Program;
import me.danielzgtg.compsci11_sem2_2017.common.serialization.SerializationUtils;

import static me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants.DEBUG;

@SuppressWarnings("WeakerAccess")
public final class GameServer extends AppContainer {

	private static final Map<String, WorldGenerator> WORLD_TYPES;
	public static final String WORLD_TYPES_DESCRIPTION;

	public final boolean isIntegrated;
	public final Program program;

	private List<PlayerServer> players = new LinkedList<>();
	private int nextPlayerID = Integer.MIN_VALUE;
	private PlayerServer turn;

	private String requestedWorldType;
	private int xSize = BattleTownConstants.WORLD_MINLENGTH;
	private int ySize = BattleTownConstants.WORLD_MINLENGTH;
	private WorldServer world;

	public final BlockingPipe<Runnable> pipe = new BlockingPipe<>();

	private final List<String> leaderboard;

	static {
		final Map<String, WorldGenerator> worldTypes = new HashMap<>();

		worldTypes.put("simplex", SimplexGenerator.INSTANCE);

		WORLD_TYPES = Collections.unmodifiableMap(worldTypes);
		WORLD_TYPES_DESCRIPTION = StringUtils
				.joinStrings(WORLD_TYPES.keySet().toArray(new String[WORLD_TYPES.size()]), ", ");
	}

	public GameServer(final boolean isIntegrated, final Program program) {
		Validate.notNull(program);

		this.isIntegrated = isIntegrated;
		this.program = program;
		this.requestedWorldType = WORLD_TYPES.keySet().iterator().next();

		this.leaderboard = loadLeaderboard();
	}

	private static final List<String> loadLeaderboard() {
		try {
			if (DEBUG) {
				Log.log("[GameServer] Loading Leaderboard.");
			}

			final Map<?, ?> fileConfig = ResourceUtils.loadConstantsMap(
					"leaderboard.dat", ResourceUtils.EXTERNAL_RESOURCE_LOADER);
			final Object leaderboard = fileConfig.get("leaderboard");

			if (leaderboard instanceof Collection) {
				final Collection leaderboard1 = (Collection) leaderboard;
				final List<String> result = new LinkedList<>();

				for (final Object o : leaderboard1) {
					if (o instanceof String) {
						result.add((String) o);
					}
				}

				while (result.size() > BattleTownConstants.MAX_LEADERBOARD_SLOTS) {
					result.remove(0);
				}

				if (DEBUG) {
					Log.log("[GameServer] Loaded Leaderboard.");
				}

				return Collections.unmodifiableList(result);
			}
			Log.log("[GameServer] Failed to load leaderboard.");
		} catch (final Exception e) {
			Log.log("[GameServer] Failed to load leaderboard.");
			Log.log(e);
		}

		return Collections.emptyList();
	}

	private static final void saveLeaderboard(final List<String> leaderboard) {
		try {
			Validate.notNull(leaderboard);

			if (DEBUG) {
				Log.log("[GameServer] Saving Leaderboard.");
			}

			final List<String> leaderboardCopy = new LinkedList<>(leaderboard);

			while (leaderboardCopy.size() > BattleTownConstants.LEADERBOARD_SLOTS) {
				leaderboardCopy.remove(0);
			}

			final Map<Object, Object> fileConfig = new HashMap<>();
			fileConfig.put("leaderboard", leaderboardCopy);

			SerializationUtils.encode(new FileOutputStream("leaderboard.dat"), fileConfig);

			if (DEBUG) {
				Log.log("[GameServer] Saved Leaderboard.");
			}
		} catch (final Exception e) {
			Log.log("[GameServer] Failed to save leaderboard.");
			Log.log(e);
		}
	}

	public final int generatePlayerID() {
		OUTER:
		while (true) {
			final int result = this.nextPlayerID++;

			for (final PlayerServer player : this.players) {
				if (player.id == result) {
					continue OUTER;
				}
			}

			return result;
		}
	}

	@Override
	protected final void doLaunch() {
		new Thread(this::gameLogic).start();
	}

	private final void gameLogic() {
		if (DEBUG) {
			Log.log("[BattleTownServer] ServerStart");
		}

		this.program.addThread(Thread.currentThread(), "ServerCore");

		this.pipe.trap(Runnable::run);
		this.stop(null);
	}

	private final void serverTick() {
		if (this.turn != null) {
			throw new AssertionError();
		}

		if (this.world != null) {
			this.world.tick();
			this.turn = this.players.get(0);
		}
	}

	public final void endTurn(final PlayerServer player) {
		Validate.notNull(player);

		if (this.turn != player) {
			player.sendChat(BattleTownConstants.REJECTMSG_NOT_TURN);
		}

		this.advanceTurn1();
		this.advanceTurn2();
	}

	private final void advanceTurn2() {
		if (this.world == null) {
			throw new IllegalStateException();
		}

		if (DEBUG) {
			Log.log("[BattleTownServer] AdvanceTurnPre2: " + this.turn);
		}

		final List<PlayerServer> players = this.players;
		if (this.turn == null) {
			if (!players.isEmpty()) {
				this.serverTick();
			}
		}

		final PlayerServer turn = this.turn;
		if (DEBUG) {
			Log.log("[BattleTownServer] AdvanceTurnPost2: " + turn);
		}

		for (final PlayerServer player : players) {
			player.indicateTurn(turn);
		}
	}

	private final void advanceTurn1() {
		if (this.world == null) {
			throw new IllegalStateException();
		}

		if (DEBUG) {
			Log.log("[BattleTownServer] AdvanceTurnPre1: " + this.turn);
		}

		final List<PlayerServer> players = this.players;
		if (this.turn != null) {
			for (final Iterator<PlayerServer> iter = players.iterator(); iter.hasNext(); ) {
				final PlayerServer player = iter.next();

				if (this.turn == player) {
					if (iter.hasNext()) {
						this.turn = iter.next();
					} else {
						this.turn = null;
						//this.serverTick();
					}
				}
			}
		}

		final PlayerServer turn = this.turn;
		if (DEBUG) {
			Log.log("[BattleTownServer] AdvanceTurnPost1: " + turn);
		}

		for (final PlayerServer player : players) {
			player.indicateTurn(turn);
		}
	}

	private final void trySpawnPlayer(final PlayerServer player, final WorldServer world) {
		if (!world.spawnPlayer(player)) {
			this.broadcastChat(String.format(BattleTownConstants.SPAWNFAILMSG, player.name));
		}
	}

	public final void loginPlayer(final PlayerServer player) {
		if (DEBUG) {
			Log.log("[BattleTownsServer] JoinPlayer: " + player.name);
		}

		if (player.name == null) {
			player.disconnect(BattleTownConstants.JOINNAMEFAILMSG);
			return;
		}

		for (final PlayerServer otherPlayer : this.players) {
			if (otherPlayer.name.equals(player.name)) {
				player.disconnect(BattleTownConstants.JOINNAMEFAILMSG);
				return;
			}
		}

		player.restart();

		final boolean jumpstart = this.players.isEmpty() && this.world != null;

		for (final PlayerServer otherPlayer : players) {
			player.sendChat(String.format(BattleTownConstants.JOININTRODUCEMSG, otherPlayer.name));
		}

		this.players.add(player);

		this.broadcastChat(String.format(BattleTownConstants.JOINMSG, player.name));

		final WorldServer world = this.world;
		if (world != null) {
			player.begin(world.xSize, world.ySize);
			world.performInitialReplication(player);
			this.trySpawnPlayer(player, world);
		}

		if (jumpstart) {
			this.advanceTurn1();
			this.advanceTurn2();
		} else {
			player.indicateTurn(this.turn);
		}

		player.run();
	}

	public void begin() {
		final WorldGenerator generator = WORLD_TYPES.get(this.requestedWorldType);

		if (DEBUG) {
			Log.log("[BattleTownsServer] TryBegin");
		}

		if (generator == null) {
			this.broadcastChat(BattleTownConstants.BEGINFAILPREFIX + BattleTownConstants.WORLDSETTINGERROR_TYPE);
			this.restart(null);
			return;
		}

		if (!WorldCommon.isValidSize(this.xSize, this.ySize)) {
			this.broadcastChat(BattleTownConstants.BEGINFAILPREFIX + BattleTownConstants.WORLDSETTINGERROR_SIZE);
			this.restart(null);
			return;
		}

		if (DEBUG) {
			Log.log("[BattleTownsServer] WorldBegin");
		}

		final WorldServer world = new WorldServer(this.xSize, this.ySize, generator, this);
		this.world = world;
		world.battleEvent.addListener(this::broadcastBattle);
		this.broadcastChat(BattleTownConstants.BEGINMSG);

		for (final PlayerServer player : this.players) {
			player.begin(this.xSize, this.ySize);
		}

		for (final PlayerServer player : this.players) {
			world.performInitialReplication(player);
		}

		for (final PlayerServer player : this.players) {
			this.trySpawnPlayer(player, world);
		}

		this.advanceTurn1();
		this.advanceTurn2();
	}

	public void restart(String reason) {
		if (DEBUG) {
			Log.log("[BattleTownsServer] Restart: " + reason);
		}

		if (this.world != null) {
			if (reason == null) {
				reason = BattleTownConstants.REASON_UNKNOWN;
			}

			this.world = null;
			this.turn = null;

			for (final PlayerServer player : this.players) {
				player.indicateTurn(null);
			}
		}

		this.broadcastChat(String.format(BattleTownConstants.RESTARTMSG, reason));

		for (final PlayerServer player : this.players) {
			player.restart();
		}
	}

	public void stop(String reason) {
		if (DEBUG) {
			Log.log("[BattleTownsServer] Stop: " + reason);
		}

		if (reason == null) {
			reason = BattleTownConstants.REASON_UNKNOWN;
		}

		for (final Iterator<PlayerServer> iterator = this.players.iterator(); iterator.hasNext();) {
			this.doKickPlayer(iterator, iterator.next(), true,
					String.format(BattleTownConstants.SHUTDOWN_KICKFORMAT, reason));
		}

		if (!this.isIntegrated) {
			this.program.stop();
		}

		this.pipe.release();
	}

	public final void broadcastChat(final String message) {
		Validate.notNull(message);

		if (DEBUG) {
			Log.log("[BattleTownsServer] BroadcastChat: " + message);
		}

		for (final PlayerServer player : this.players) {
			player.sendChat(message);
		}
	}

	public final void informReady(final PlayerServer sender) {
		Validate.notNull(sender);
		Validate.require(sender.isReady());

		if (DEBUG) {
			Log.log("[BattleTownsServer] InformReady: " + sender.name);
		}

		for (final PlayerServer player : this.players) {
			if (!player.isReady()) {
				if (DEBUG) {
					Log.log("[BattleTownsServer] Round start is blocked by: " + player.name);
				}

				return;
			}
		}

		this.begin();
	}

	public final void requestWorldSize(final PlayerServer sender, final int xSize, final int ySize) {
		Validate.notNull(sender);

		if (DEBUG) {
			Log.log("[BattleTownsServer] WorldSizeRequest " + xSize + ", " + ySize + ", " + sender.name);
		}

		if (WorldCommon.isValidSize(xSize, ySize)) {
			this.xSize = xSize;
			this.ySize = ySize;

			this.restart(String.format(BattleTownConstants.WORLDSETTINGCHANGE_SIZE, sender.name, xSize, ySize));
		} else {
			sender.sendChat(BattleTownConstants.WORLDSETTINGERROR_SIZE);
		}
	}

	public final void requestWorldType(final PlayerServer sender, final String type) {
		Validate.notNull(sender);
		Validate.notNull(type);

		if (DEBUG) {
			Log.log("[BattleTownsServer] WorldTypeRequest " + type + ", " + sender.name);
		}

		if (WORLD_TYPES.containsKey(type)) {
			this.requestedWorldType = type;

			this.restart(String.format(BattleTownConstants.WORLDSETTINGCHANGE_TYPE, sender.name, type));
		} else {
			sender.sendChat(BattleTownConstants.WORLDSETTINGERROR_TYPE);
		}
	}

	public final void receiveChat(final PlayerServer sender, final String message) {
		Validate.notNull(sender);
		Validate.notNull(message);

		if (DEBUG) {
			Log.log("[BattleTownsServer] ReceiveChat " + sender.name + ", " + message);
		}

		if (message.length() > BattleTownConstants.CHAT_MAXLENGTH) {
			this.kickPlayer(sender, BattleTownConstants.KICKREASON_CHAT);
			return;
		}

		if (message.startsWith("/")) {
			if ("/stop".equals(message)) {
				this.stop(String.format(BattleTownConstants.SHUTDOWNREASON_PLAYERSHUTDOWN, sender.name));
			} else if ("/restart".equals(message)) {
				this.restart(String.format(BattleTownConstants.RESTARTREASON_PLAYERRESET, sender.name));
			} else if ("/limits".equals(message)) {
				sender.sendChat(String.format(BattleTownConstants.LIMITS_CHAT_FORMAT,
						BattleTownConstants.WORLD_MINLENGTH, BattleTownConstants.WORLD_MAXLENGTH,
						BattleTownConstants.CHAT_MAXLENGTH, WORLD_TYPES_DESCRIPTION));
			} else if (message.startsWith("/kick ")) {
				final String target = message.substring(6);

				if (!PlayerServer.isValidName(target)) {
					sender.sendChat("Kick: Not a valid player!");
					return;
				}

				for (final Iterator<PlayerServer> iter = this.players.iterator(); iter.hasNext();) {
					final PlayerServer player = iter.next();

					if (player.name.equals(target)) {
						this.doKickPlayer(iter, player, false,
								String.format(BattleTownConstants.KICKREASON_PLAYERKICK, sender.name));
						return;
					}
				}

				sender.sendChat("Kick: Player not found!");
			} else if ("/top".equals(message)) {
				final List<String> leaderboard = this.leaderboard;
				sender.sendChat(String.format(BattleTownConstants.LEADERBOARD_HEADER, leaderboard.size()));

				for (final String s : leaderboard) {
					sender.sendChat(String.format(BattleTownConstants.LEADERBOARD_FORMAT, s));
				}
			} else if ("/cheat".equals(message)) {
				if (this.world == null) {
					sender.sendChat(BattleTownConstants.REJECTMSG_NO_WORLD);
					return;
				}

				sender.grantVictory();
			} else {
				sender.sendChat("Unknown command.");
			}
		} else {
			this.broadcastChat(String.format(BattleTownConstants.CHAT_FORMAT, sender.name, message));
		}
	}

	public final void updateTile(final TileNetwork update) {
		Validate.notNull(update);

		if (DEBUG) {
			Log.log("[BattleTownsServer] Relaying World Update: " + update);
		}

		for (final PlayerServer player : this.players) {
			player.sendUpdate(update);
		}
	}

	public final void grantVictory(final PlayerServer player) {
		Validate.notNull(player);

		if (DEBUG) {
			Log.log("[BattleTownsServer] GrantVictory: " + player);
		}

		final List<String> leaderboard = this.leaderboard;
		leaderboard.add(player.name);

		while (leaderboard.size() > BattleTownConstants.MAX_LEADERBOARD_SLOTS) {
			leaderboard.remove(0);
		}

		saveLeaderboard(this.leaderboard);

		this.broadcastChat(String.format(BattleTownConstants.VICTORYMSG, player.name));
	}

	public final void requestAction(final PlayerServer player,
			final int x, final int y, final boolean building,
			final String action) {
		Validate.notNull(player);
		Validate.notNull(action);

		if (DEBUG) {
			Log.log(String.format("[BattleTownsServer] ActionRequest (%d,%d) %s \"%s\" for %s",
					x, y, building ? "building" : "unit", action, player.toString()));
		}

		if (this.world == null) {
			player.sendChat(BattleTownConstants.REJECTMSG_NO_WORLD);
			return;
		}

		if (this.turn != player) {
			player.sendChat(BattleTownConstants.REJECTMSG_NOT_TURN);
		}

		if (!this.world.isValidTile(x, y)) {
			player.sendChat(BattleTownConstants.REJECTMSG_INVALID_POSITION);
			return;
		}

		final TileServer tile = this.world.getTile(x, y);
		Validate.notNull(tile);

		final TileAdornment adornment = building ? tile.getBuilding() : tile.getUnit();

		if (adornment == null) {
			player.sendChat(BattleTownConstants.REJECTMSG_EMPTY_LAYER);
			return;
		}

		if (!player.equals(adornment.getOwner())) {
			player.sendChat(BattleTownConstants.REJECTMSG_NOT_OWNER);
			return;
		}

		if (!adornment.isValidAction(action)) {
			player.sendChat(BattleTownConstants.REJECTMSG_INVALID_ACTION);
			return;
		}

		adornment.performAction(action);
	}

	public final void broadcastBattle(final BattleResult battle) {
		Validate.notNull(battle);

		if (DEBUG) {
			Log.log("[BattleTownsServer] Relaying Battle: " +  battle.toString());
		}

		for (final PlayerServer player : this.players) {
			player.sendBattle(battle);
		}
	}

	public final void performBattle(final PlayerServer player, final boolean simulation,
			final int unitX, final int unitY, final int targetX, final int targetY, final boolean move) {
		Validate.notNull(player);

		if (unitX == targetX && unitY == targetY) {
			return;
		}

		final WorldServer world = this.world;
		if (world == null) {
			player.sendChat(BattleTownConstants.REJECTMSG_NO_WORLD);
			return;
		}

		if (this.turn != player && !simulation) {
			player.sendChat(BattleTownConstants.REJECTMSG_NOT_TURN);
		}

		if (!(world.isValidTile(unitX, unitY) && world.isValidTile(targetX, targetY))) {
			player.sendChat(BattleTownConstants.REJECTMSG_INVALID_POSITION);
			return;
		}

		final TileServer unitTile = world.getTile(unitX, unitY);
		final TileServer target = world.getTile(targetX, targetY);
		Validate.notNull(unitTile);

		final UnitServer unit = unitTile.getUnit();

		if (unit == null) {
			player.sendChat(BattleTownConstants.REJECTMSG_EMPTY_LAYER);
			return;
		}

		if (!player.equals(unit.getOwner())) {
			player.sendChat(BattleTownConstants.REJECTMSG_NOT_OWNER);
			return;
		}

		final BattleResult result = unit.fight(target, simulation, move);

		if (result != null) {
			if (simulation) {
				player.replySimulation(result);
			} else {
			}
		}
	}

	public final void kickPlayer(final PlayerServer player, final String reason) {
		Validate.notNull(player);

		if (DEBUG) {
			Log.log("[BattleTownsServer] KickPlayer: " + player.name);
		}

		final Iterator<PlayerServer> iter = this.players.iterator();
		while (iter.hasNext()) {
			final PlayerServer get = iter.next();

			if (get == player) {
				this.doKickPlayer(iter, player, false, reason);

				return;
			}
		}

		Log.log("[BattleTownsServer] Player is not in this server!: "+  player.toString());
	}

	private final void doKickPlayer(final Iterator<PlayerServer> iter, final PlayerServer player,
			final boolean quiet, final String reason) {
		Validate.notNull(iter);
		Validate.notNull(player);

		if (DEBUG) {
			Log.log("[BattleTownsServer] doKickPlayer: " + player.name);
		}

		final boolean advance = this.turn == player;
		if (advance) {
			this.advanceTurn1();
		}

		iter.remove();

		player.disconnect(String.format(BattleTownConstants.KICKREASON_DISCONNECTMSG,
				reason == null ? BattleTownConstants.REASON_UNKNOWN : reason));

		final WorldServer world = this.world;
		if (world != null) {
			world.helpRemovePlayer(player);
		}

		if (!quiet) {
			this.broadcastChat(String.format(BattleTownConstants.KICKREASON_BROADCASTMSG,
					player.name, reason == null ? BattleTownConstants.REASON_UNKNOWN : reason));
		}

		if (this.isIntegrated) {
			this.stop("IntegratedTeardown");
		} else {
			if (advance) {
				this.advanceTurn2();
			}
		}
	}

	public final boolean isIntegrated() {
		return this.isIntegrated;
	}

	public final List<PlayerServer> getPlayers() {
		return Collections.unmodifiableList(new ArrayList<>(this.players));
	}

	public final WorldServer getWorld() {
		return this.world;
	}
}
