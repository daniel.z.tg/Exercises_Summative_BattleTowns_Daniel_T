package me.danielzgtg.compsci11_sem2_2017.battletowns.server;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Event;

import static me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants.DEBUG;

public abstract class PlayerServer implements Runnable {

	public final String name;
	public final GameServer server;
	public final int id;
	private boolean ready;
	private boolean begun;

	public final Event<Pair<Resource, Integer>> resourceUpdateEvent = new Event<>();
	private /*mut*/ final int[] resources = new int[BattleTownConstants.NUM_RESOURCES];

	public PlayerServer(final String name, final GameServer server) {
		Validate.require(isValidName(name));
		Validate.notNull(server);

		this.name = name;
		this.server = server;
		this.id = server.generatePlayerID();
	}

	protected final void handleChat(final String message) {
		this.server.pipe.send(() -> this.server.receiveChat(this, message), false);
	}

	protected final void handlePass() {
		this.server.pipe.send(() -> this.server.endTurn(this), false);
	}

	protected final void handleWorldSizeRequest(final int xSize, final int ySize) {
		this.server.pipe.send(() -> this.server.requestWorldSize(this, xSize, ySize), false);
	}

	protected final void handleWorldTypeRequest(final String type) {
		this.server.pipe.send(() -> this.server.requestWorldType(this, type), false);
	}

	protected final void handleAction(final int x, final int y, final boolean building, final String action) {
		this.server.pipe.send(() -> this.server.requestAction(this, x, y, building, action), false);
	}

	protected final void handleBattle(final boolean simulation,
			final int unitX, final int unitY, final int targetX, final int targetY, final boolean move) {
		this.server.pipe.send(() ->
				this.server.performBattle(this, simulation, unitX, unitY, targetX, targetY, move), false);
	}

	/*packaged*/ final void begin(final int xSize, final int ySize) {
		if (!WorldServer.isValidSize(xSize, ySize)) {
			throw new IllegalArgumentException();
		}

		if (DEBUG) {
			Log.log("[BattleTownsPlayerServer] Begin sent: " + this.name);
		}

		this.begin0(xSize, ySize);
		this.begun = true;
	}

	private final void begin0(final int xSize, final int ySize) {
		// 0 and 0 means world destroyed
		//		if (this.begun) {
		BattleTownConstants.copyResources(this.resources);

		for (final Resource resource : Resource.RESOURCE_LIST) {
			this.sendResource(resource, this.getResource(resource));
		}
		//		}

		this.doBegin(xSize, ySize);
	}

	protected abstract void doBegin(final int xSize, final int ySize);

	/*packaged*/ final void restart() {
		if (DEBUG) {
			Log.log("[BattleTownPlayerServer] Finish sent: " + this.name);
		}

		this.ready = false;
		this.begin0(0, 0);
		this.begun = false;
	}

	public final boolean isReady() {
		return this.ready;
	}

	public final void makeReady() {
		if (DEBUG) {
			Log.log("[PlayerServer] Make ready request received: " + this.name);
		}

		if (!this.ready) {
			Log.log("[PlayerServer] Making player ready: " + this.name);
			this.server.pipe.send(() -> {
				if (!this.ready) {
					this.ready = true;
					this.server.informReady(this);
				}
			}, false);
		} else {
			if (DEBUG) {
				Log.log("[PlayerServer] Request already met, was already ready!: " + this.name);
			}
		}
	}

	public final int getResource(final Resource resource) {
		Validate.notNull(resource);

		return this.resources[resource.ordinal()];
	}

	public final void setResource(final Resource resource, final int amount) {
		Validate.notNull(resource);

		if (DEBUG) {
			Log.log("[PlayerServer] Will set resource: " + resource.name() + " to " + amount + " for " + this.name);
		}

		this.resources[resource.ordinal()] = amount;
		this.sendResource(resource, amount);
	}

	public static final boolean isValidName(final String name) {
		return StringUtils.containsNonWhitespace(name) && !"system".equals(name);
	}

	public final void grantVictory() {
		this.server.grantVictory(this);
		this.server.restart(String.format(BattleTownConstants.RESTARTREASON_VICTORY, this.name));
	}

	public final void sendChat(final String message) {
		if (DEBUG) {
			Log.log("[PlayerServer] SendChat: (" + this.name + "): " + message);
		}

		this.doSendChat(message);
	}

	public abstract void doSendChat(final String message);

	/*packaged*/ final void disconnect(final String reason) {
		if (DEBUG) {
			Log.log("[PlayerServer] Disconnect: (" + this.name + "): " + reason);
		}

		this.doDisconnect(reason);
	}

	protected abstract void doDisconnect(final String reason);

	/*packaged*/ final void indicateTurn(final PlayerServer player) {
		final String playerName = player == this ? "" : player == null ? "system" : player.name;

		if (DEBUG) {
			Log.log(player == this ? "[PlayerServer] IndicateMyTurn: " + this.name :
					"[PlayerServer] IndicateTurn: (" + this.name + "): " + playerName);
		}

		this.doIndicateTurn(playerName);
	}

	protected abstract void doIndicateTurn(final String playerName);

	public final void sendUpdate(final TileNetwork update) {
		Validate.notNull(update);

		if (DEBUG) {
			Log.log("[PlayerServer] TileUpdate (" + this.name + "): " + update.toString());
		}

		this.doSendUpdate(update);
	}

	protected abstract void doSendUpdate(final TileNetwork update);

	/*packaged*/ final void sendBattle(final BattleResult battle) {
		Validate.notNull(battle);

		if (DEBUG) {
			Log.log("[PlayerServer] BattleSend (" + this.name + "): " + battle.toString());
		}

		this.doSendBattle(battle);
	}

	protected abstract void doSendBattle(final BattleResult battle);

	/*packaged*/ final void replySimulation(final /*nullable*/ BattleResult battle) {
		if (DEBUG) {
			Log.log("[PlayerServer] SimulationReply (" + this.name + "): " + battle.toString());
		}

		this.doReplySimulation(battle);
	}

	protected abstract void doReplySimulation(final BattleResult battle);

	private final void sendResource(final Resource resource, final int amount) {
		Validate.notNull(resource);

		if (DEBUG) {
			Log.log("[PlayerServer] ResourceUpdate: " + resource.name() + " to " + amount + " for " + this.name);
		}

		this.doSendResource(resource, amount);
	}

	protected abstract void doSendResource(final Resource resource, final int amount);

	@Override
	public String toString() {
		return "PlayerServer: { server=" + this.server + ", id=" + this.id + ", name=" +
				this.name + ", ready=" + this.ready + " }";
	}
}
