package me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base;

import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.WorldServer;

public abstract interface WorldGenerator {

	public abstract TileServer generateTile(final int x, final int y, final WorldServer world);
}
