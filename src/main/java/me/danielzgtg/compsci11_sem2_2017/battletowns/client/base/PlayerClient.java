package me.danielzgtg.compsci11_sem2_2017.battletowns.client.base;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.GameClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.WorldCommon;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

import static me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants.DEBUG;

public abstract class PlayerClient implements Runnable {

	private String turnName;
	private TileNetwork[][] tiles;
	private int xSize;
	private int ySize;
	private int[] localResources = new int[Resource.RESOURCE_LIST.size()];
	private boolean localReady;
	public final GameClient client;

	public PlayerClient(final GameClient client) {
		Validate.notNull(client);

		this.client = client;
	}

	public final String getTurn() {
		return this.turnName;
	}

	public final GameClient getClient() {
		return this.client;
	}

	public final void chat(final String message) {
		Validate.notNull(message);

		if (DEBUG) {
			Log.log("[PlayerClient] SendChat: " + message);
		}

		this.doChat(message);
	}

	protected abstract void doChat(final String message);

	public final void makeReady() {
		if (DEBUG) {
			Log.log("[PlayerClient] MakeReady");
		}

		this.doMakeReady();
	}

	protected abstract void doMakeReady();

	public final void pass() {
		if (DEBUG) {
			Log.log("[PlayerClient] Pass");
		}

		this.doPass();
	}

	protected abstract void doPass();

	public final void requestWorldSize(final int xSize, final int ySize) {
		if (!WorldCommon.isValidSize(xSize, ySize)) {
			throw new IllegalArgumentException();
		}

		this.doRequestWorldSize(xSize, ySize);
	}

	protected abstract void doRequestWorldSize(final int xSize, final int ySize);

	public final void requestWorldType(final String type) {
		Validate.notNull(type);

		this.doRequestWorldType(type);
	}

	protected abstract void doRequestWorldType(final String type);

	public final boolean isClientReady() {
		return this.localReady;
	}

	public final int getClientResource(final Resource resource) {
		Validate.notNull(resource);

		return this.localResources[resource.ordinal()];
	}

	public final TileNetwork getClientTile(final int x, final int y) {
		if (this.tiles == null) {
			throw new IllegalStateException("World does not exist!");
		} else if (x < 0 || y < 0 || x >= this.xSize || y >= this.ySize) {
			throw new ArrayIndexOutOfBoundsException();
		}

		final TileNetwork result = this.tiles[x][y];

		if (result == null) {
			throw new AssertionError("Null Tile!");
		}

		return result;
	}

	public final void performAction(final int x, final int y, final boolean building, final String action) {
		if (this.tiles == null) {
			Log.log("[PlayerClient] Refused to send action: World does not exist to perform action.");
			return;
		}

		if (x < 0 || y < 0 || x >= this.xSize || y >= this.ySize) {
			Log.log("[PlayerClient] Refused to send action: Position does not exist in world.");
			return;
		}

		final TileNetwork tile = this.tiles[x][y];

		if (tile == null) {
			Log.log("[PlayerClient] Refused to send action: Still waiting for tile replication.");
			return;
		}

		if ((building ? tile.getBuildingData() : tile.getUnitData()).length() == 0) {
			Log.log("[PlayerClient] Refused to send action: Tile adornment does not exist on tile.");
			return;
		}

		this.doPerformAction(x, y, building, action);
	}

	protected abstract void doPerformAction(final int x, final int y, final boolean building, final String action);

	public final void performBattle(final boolean simulation,
			final int unitX, final int unitY, final int targetX, final int targetY, final boolean move) {
		this.doPerformBattle(simulation, unitX, unitY, targetX, targetY, move);
	}

	protected abstract void doPerformBattle(final boolean simulation,
			final int unitX, final int unitY, final int targetX, final int targetY, final boolean move);

	public final boolean hasClientWorld() {
		return this.tiles != null;
	}

	public final int getClientXSize() {
		return this.xSize;
	}

	public final int getClientYSize() {
		return this.ySize;
	}

	protected final void handleBegin(final int xSize, final int ySize) {
		if (xSize < 0 || ySize < 0) {
			throw new IllegalArgumentException();
		}

		if ((xSize | ySize) == 0) {
			if (DEBUG) {
				Log.log("[PlayerClient] DoFinish");
			}

			if (this.tiles != null) {
				if (DEBUG) {
					Log.log("[PlayerClient] ClearTiles");

					for (int x = 0; x < this.xSize; x++) {
						for (int y = 0; y < this.ySize; y++) {
							Log.log("[PlayerClient] FinishTile: " + x + ", " + y + ", "
									+ String.valueOf(this.tiles[x][y]));
						}
					}
				}

				this.tiles = null;
				this.handleReset();
			}

			this.localReady = false;
		} else {
			if (DEBUG) {
				Log.log("[PlayerClient] DoBegin " + xSize + ", " + ySize);
			}

			this.xSize = xSize;
			this.ySize = ySize;

			this.tiles = new TileNetwork[xSize][ySize];

			this.doHandleBegin(xSize, ySize);
		}
	}

	protected abstract void handleReset();

	protected abstract void doHandleBegin(final int xSize, final int ySize);

	protected void handleTurn(final String playerName) {
		Validate.notNull(playerName);

		if (DEBUG) {
			Log.log("[PlayerClient] It is now " + (playerName.length() == 0 ? "my turn" : playerName + "'s turn"));
		}

		this.turnName = playerName;

		this.doHandleTurn(playerName);
	}

	protected abstract void doHandleTurn(final String playerName);

	protected final void handleUpdate(final TileNetwork update) {
		if (DEBUG) {
			Log.log("[PlayerClient] TileUpdate: " + update.toString());
		}

		if (this.tiles == null) {
			Log.log("[PlayerClient] World does not exist?");
			return;
		}

		final TileNetwork[] subtiles = this.tiles[update.x];
		if (DEBUG) {
			Log.log("[PlayerClient] TileReplaced: " + String.valueOf(subtiles[update.y]));
		}
		subtiles[update.y] = update;

		this.doHandleUpdate(update);
	}

	protected abstract void doHandleUpdate(final TileNetwork update);

	protected final void handleResource(final Resource resource, final int amount) {
		Validate.notNull(resource);

		this.localResources[resource.ordinal()] = amount;

		this.doHandleResource(resource, amount);
	}

	protected abstract void doHandleResource(final Resource resource, final int amount);

	public final void disconnect() {
		this.doDisconnect();
	}

	protected abstract void doDisconnect();
}
