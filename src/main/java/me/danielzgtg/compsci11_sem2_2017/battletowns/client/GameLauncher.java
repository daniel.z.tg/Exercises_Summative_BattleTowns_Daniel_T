package me.danielzgtg.compsci11_sem2_2017.battletowns.client;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BooleanSupplier;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.base.NetworkPlayerClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleTownConfig;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.GameServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.NetworkPlayerServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.PlayerServer;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl.MainMenuGraphicalAppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.io.MemoryPipe;
import me.danielzgtg.compsci11_sem2_2017.common.platform.MainThreadUtils;
import me.danielzgtg.compsci11_sem2_2017.common.platform.PlatformUtils;
import me.danielzgtg.compsci11_sem2_2017.common.program.Program;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;

public final class GameLauncher {

	/**
	 * Options in main  menu.
	 */
	private static final List<Pair<String, Pair<String, BooleanSupplier>>> MAIN_MENU_CONTROLS;

	static {
		final List<Pair<String, Pair<String, BooleanSupplier>>> tmpMainMenuControls = new LinkedList<>();

		tmpMainMenuControls.add(new Pair<>("battletowns.playButton", new Pair<>(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.play", () -> {
			gameSelector();
			MainThreadUtils.release();

			return true;
		})));

		//		tmpMainMenuControls.add(new Pair<>("slider.configButton", new Pair<>(
		//				"slider.settings", () -> {
		//			launchConfig();
		//
		//			return true;
		//		})));

		MAIN_MENU_CONTROLS = Collections.unmodifiableList(tmpMainMenuControls);
	}

	public static final void mainMenu() {
		MainThreadUtils.require();
		new MainMenuGraphicalAppContainer(BattleTownConfig.BATTLETOWNS_CONSTANT_DATA, MAIN_MENU_CONTROLS,
				(frame) -> { SwingUtils.hookJFrameClosing(frame, () -> {
					MainThreadUtils.release();

					return true;
				}); }).launch();
	}

	public static final void gameSelector() {
		new GameConnector().launch();
	}

	public static final void singleplayer() {
		final Program program = new Program("BattleTowns");
		final GameServer server = new GameServer(true, program);

		final MemoryPipe pipe1 = new MemoryPipe();
		final MemoryPipe pipe2 = new MemoryPipe();

		final GameClient client = new GameClient((game) -> new NetworkPlayerClient(game, pipe1.in, pipe2.out), program);
		final PlayerServer serverPlayer = new NetworkPlayerServer(
				String.valueOf(BattleTownConfig.BATTLETOWNS_CONFIG.get("battletowns.playername"))
				/*"localplayer"*/,
				server, pipe2.in, pipe1.out);

		server.launch();
		server.loginPlayer(serverPlayer);

		client.launch();
	}

	public static void main(final String[] ignore) {
		PlatformUtils.setHighPerformance(true);

		if (PlatformUtils.getHighPerformance()) {
			SwingUtils.setupSystemLookAndFeel();
		}

		//		mainMenu();
		MainThreadUtils.trap(GameLauncher::mainMenu);
	}
}
