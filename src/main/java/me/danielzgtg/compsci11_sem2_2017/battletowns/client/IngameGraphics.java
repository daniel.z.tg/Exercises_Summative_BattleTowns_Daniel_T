package me.danielzgtg.compsci11_sem2_2017.battletowns.client;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import java.io.IOException;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.WorldClient;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDB;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.MainThreadUtils;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.program.Program;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.Camera;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.CameraShaderData;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.GLUtils;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.GraphicalBuffers;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.GraphicalSystem;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.GraphicalWindow;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.Shader;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.Texture;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Model;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Quad;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_Q;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_R;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_Z;
import static org.lwjgl.glfw.GLFW.glfwGetTime;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glViewport;

public final class IngameGraphics {

	private Shader shader;
	public MemoryDB<Model> models = new MemoryDB<>();
	private Model cursor;
	private Texture tex;
	private Camera camera;
	private GraphicalBuffers buffers = new GraphicalBuffers();
	private WorldClient world;
	public static final int SCR_WIDTH = 640;
	public static final int SCR_HEIGHT = 480;
	public final GraphicalWindow window;
	private volatile boolean closed;
	private volatile boolean allowFullStop;
	private Program program;
	public final List<Runnable> jobs = new LinkedList<>();
	private final List<Runnable> jobs2 = new LinkedList<>();

	public IngameGraphics(final Program program, final WorldClient world) {
		Validate.notNull(program);
		Validate.notNull(world);

		GraphicalSystem.require();

		this.window = GraphicalWindow.create(SCR_WIDTH, SCR_HEIGHT, "Battle Towns", true);
		this.window.keyPressEvent.addListener((data) -> {
			if (/*key*/ data.getLeft() == GLFW_KEY_ESCAPE) {
				this.program.stop();
			}
		});

		this.program = program;
		this.world = world;
	}

	public void close() {
		this.closed = true;
	}

	public void fullStop() {
		this.allowFullStop = true;
	}

	public void run() {
		try {
			this.doRun();
		} catch (final Exception e) {
			Log.log("[IngameGraphics] Render Thread death!");
			Log.log(e);
			throw new RuntimeException(e);
		}
	}

	private final void doRun() {
		this.program.addThread(Thread.currentThread(), "Render thread");

		this.window.bind();

		double currentTime;
		double lastTime;
		double elapsedTime;

		lastTime = 0;

		this.init();

		lastTime = glfwGetTime() - 1;
		while (!this.window.updateShouldStop(this.program) && !this.closed) {
			currentTime = glfwGetTime();
			elapsedTime = currentTime - lastTime;
			lastTime = currentTime;

			this.animate(elapsedTime);
			this.render(elapsedTime);

			this.window.flipBuffers();
		}

		boolean interrupted = false;
		while (!this.allowFullStop && program.running()) {
			try {
				Thread.sleep(2L);
			} catch (final Exception e) {
				interrupted = true;
			}
		}

		if (interrupted) {
			Thread.currentThread().interrupt();
		}

		this.cleanup();

		this.window.cleanup();
		GraphicalSystem.release();
	}

	//	private static final float[][] vertices = new float[][] {
	//			//          x,     y,     z,      r,    g,    b,    a,     u,    v
	//			new float[] {
	//					-0.8f, +0.8f, -1.0f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 0.0f
	//			},
	//			new float[] {
	//					-0.8f, -0.8f, -1.0f,   0.0f, 0.0f, 1.0f, 1.0f,  0.0f, 1.0f
	//			},
	//			new float[] {
	//					+0.8f, -0.8f, -1.0f,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 1.0f
	//			},
	//			new float[] {
	//					+0.8f, +0.8f, -1.0f,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 0.0f
	//			}
	//	};

	private static final float[][][] CURSOR_VERTICES = new float[][][] {
			new float[][] {
					//          x,     y,     z,      r,    g,    b,    a,     u,    v
					new float[] {
							-0.1f, +0.2f, -0.1f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							-0.1f, +0.2f, +1.1f,   1.0f, 0.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							+0.1f, +0.2f, +1.1f,   1.0f, 0.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							+0.1f, +0.2f, -0.1f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//          x,     y,     z,      r,    g,    b,    a,     u,    v
					new float[] {
							-0.1f, +0.2f, +0.9f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							-0.1f, +0.2f, +1.1f,   1.0f, 0.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							+1.1f, +0.2f, +1.1f,   1.0f, 0.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							+1.1f, +0.2f, +0.9f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//          x,     y,     z,      r,    g,    b,    a,     u,    v
					new float[] {
							+1.1f, +0.2f, +1.1f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							+1.1f, +0.2f, -0.1f,   1.0f, 0.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							+0.9f, +0.2f, -0.1f,   1.0f, 0.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							+0.9f, +0.2f, +1.1f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//          x,     y,     z,      r,    g,    b,    a,     u,    v
					new float[] {
							+1.1f, +0.2f, -0.1f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							-0.1f, +0.2f, -0.1f,   1.0f, 0.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							-0.1f, +0.2f, +0.1f,   1.0f, 0.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							+1.1f, +0.2f, +0.1f,   1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
	};

	public void init() {
		this.window.keyPressEvent.addListener(this::keyDown);
		this.window.keyReleaseEvent.addListener(this::keyUp);

		glClearColor(0.4f, 0.6f, 0.9f, 0f);
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);

		try {
			this.shader = Shader.of(ResourceUtils.readAsString(
					"newgraphics/NewGraphics.vsh", GLUtils.NEWGRAPHICS_RESOURCE_LOADER),
					ResourceUtils.readAsString(
							"newgraphics/NewGraphics.fsh", GLUtils.NEWGRAPHICS_RESOURCE_LOADER),
					Arrays.asList("projM", "viewM", "modelM"));
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}

		this.shader.reupload();

		glEnable(GL_DEPTH_TEST);

		this.camera = new Camera(new CameraShaderData(this.shader), this.buffers,
				0.1F, 100F, 60, (float) SCR_WIDTH / (float) SCR_HEIGHT);

		this.camera.setPitch(45);

		this.cursor = Model.builder().quad().quad().quad().quad().build();
		this.updateCursor();

		for (int i = 0; i < CURSOR_VERTICES.length; i++) {
			final Quad quad = ((Quad) cursor.getPrimitives().get(i));
			final float[][] primitiveVertices = CURSOR_VERTICES[i];
			for (int j = 0; j < primitiveVertices.length; j++) {
				quad.setVertex(primitiveVertices[j], j);
			}
		}

		this.cursor.reupload();
		this.cursor.flipBuffers();

		//		final byte[] texData = new byte[]
		//				{
		//						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
		//						(byte) 0xFF, (byte) 0xAA, (byte) 0xAA, (byte) 0xFF,
		//						(byte) 0xFF, (byte) 0xAA, (byte) 0xAA, (byte) 0xFF,
		//						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
		//
		//						(byte) 0xFF, (byte) 0xAA, (byte) 0xAA, (byte) 0xFF,
		//						(byte) 0x00, (byte) 0xFF, (byte) 0xAA, (byte) 0xFF,
		//						(byte) 0xAA, (byte) 0xAA, (byte) 0xFF, (byte) 0xFF,
		//						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
		//
		//						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
		//						(byte) 0xAA, (byte) 0xAA, (byte) 0xFF, (byte) 0xFF,
		//						(byte) 0xFF, (byte) 0xFF, (byte) 0xAA, (byte) 0xFF,
		//						(byte) 0xAA, (byte) 0xAA, (byte) 0xFF, (byte) 0xFF,
		//
		//						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
		//						(byte) 0xFF, (byte) 0xAA, (byte) 0xFF, (byte) 0xFF,
		//						(byte) 0xAA, (byte) 0xAA, (byte) 0xFF, (byte) 0xFF,
		//						(byte) 0xFF, (byte) 0xAA, (byte) 0xFF, (byte) 0xFF,
		//				};

		this.tex = Texture.loadWhite();
		this.tex.reupload();
	}

	private void updateVerticies() {
	}

	public void animate(final double delta) {
		this.handleMovement(delta);
		this.updateVerticies();
		synchronized (this.jobs) {
			jobs2.addAll(this.jobs);
			this.jobs.clear();
		}
		for (final Runnable job : jobs2) {
			job.run();
		}
		jobs2.clear();
	}

	public void render(final double delta) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		this.shader.bind();

		this.camera.flipBuffers();

		this.tex.bind();
		this.camera.lookThrough(this.cursor);
		this.cursor.draw();
		for (final Model model : models) {
			this.camera.lookThrough(model);
			model.draw();
		}

		GLUtils.assertNoError();
	}

	public void cleanup() {
		synchronized (jobs) {
			for (final Runnable job : jobs) {
				job.run();
			}
		}

		this.shader.cleanup();

		this.cursor.cleanup();

		this.tex.cleanup();
		this.tex = null;
	}

	//	private static final double WALK_SPEED = 0.1F;
	//	private static final double MOUSE_SPEED = 1.0F;
	//	private static final double ROT_SPEED = 60.0F;

	private void handleMovement(double delta) {
		//		final float yaw = this.camera.getYaw();
		//		double chgX = 0, chgY = 0, chgZ = 0, chgP = 0, chgW = 0;
		//
		//		if (this.keyW) {
		//			chgX += Math.sin(Math.toRadians(yaw));
		//			chgZ -= Math.cos(Math.toRadians(yaw));
		//		}
		//		if (this.keyA) {
		//			chgX -= Math.cos(Math.toRadians(yaw));
		//			chgZ -= Math.sin(Math.toRadians(yaw));
		//		}
		//		if (this.keyS) {
		//			chgX -= Math.sin(Math.toRadians(yaw));
		//			chgZ += Math.cos(Math.toRadians(yaw));
		//		}
		//		if (this.keyD) {
		//			chgX += Math.cos(Math.toRadians(yaw));
		//			chgZ += Math.sin(Math.toRadians(yaw));
		//		}
		//		if (this.keyQ) {
		//			chgY += WALK_SPEED;
		//		}
		//		if (this.keyZ) {
		//			chgY -= WALK_SPEED;
		//		}
		//		if (this.keyCamW) {
		//			chgP -= MOUSE_SPEED;
		//		}
		//		if (this.keyCamS) {
		//			chgP += MOUSE_SPEED;
		//		}
		//		if (this.keyCamA) {
		//			chgW -= MOUSE_SPEED;
		//		}
		//		if (this.keyCamD) {
		//			chgW += MOUSE_SPEED;
		//		}
		//
		//		double norm = Math.sqrt((chgX * chgX) + (chgY * chgY) + (chgZ * chgZ)) / WALK_SPEED / delta / 100;
		//
		//		if (norm != 0) {
		//			this.camera.setPos(
		//					(float) (this.camera.getPosX() + (chgX / norm)),
		//					(float) (this.camera.getPosY() + (chgY / norm)),
		//					(float) (this.camera.getPosZ() + (chgZ / norm)));
		//		}
		//
		//		if (chgP != 0 || chgW != 0) {
		//			this.camera.setRot(
		//					(float) (this.camera.getYaw() + chgW * delta * ROT_SPEED),
		//					(float) (this.camera.getPitch() + chgP * delta * ROT_SPEED));
		//		}
	}

	//	private boolean keyW, keyA, keyS, keyD, keyQ, keyZ, keyCamW, keyCamA, keyCamS, keyCamD;

	private final void keyDown(final Pair<Integer, Integer> data) {
		this.onKey(data.getLeft(), true);
	}

	private final void keyUp(final Pair<Integer, Integer> data) {
		this.onKey(data.getLeft(), false);
	}

	private final void onKey(final int key, final boolean down) {
		if (!down) {
			return;
		}

		final WorldClient world = this.world;

		switch (key) {
			//			case GLFW_KEY_W:
			//				this.keyW = down;
			//				break;
			//			case GLFW_KEY_A:
			//				this.keyA = down;
			//				break;
			//			case GLFW_KEY_S:
			//				this.keyS = down;
			//				break;
			//			case GLFW_KEY_D:
			//				this.keyD = down;
			//				break;
			//			case GLFW_KEY_Q:
			//				this.keyQ = down;
			//				break;
			//			case GLFW_KEY_Z:
			//				this.keyZ = down;
			//				break;
			case GLFW_KEY_UP:
				synchronized (this.jobs) {
					if (world.setSelection(world.getSelectionX(), world.getSelectionY() - 1)) {
						this.updateCursor();
					}
				}
				break;
			case GLFW_KEY_DOWN:
				synchronized (this.jobs) {
					if (world.setSelection(world.getSelectionX(), world.getSelectionY() + 1)) {
						this.updateCursor();
					}
				}
				break;
			case GLFW_KEY_LEFT:
				synchronized (this.jobs) {
					if (world.setSelection(world.getSelectionX() - 1, world.getSelectionY())) {
						this.updateCursor();
					}
				}
				break;
			case GLFW_KEY_RIGHT:
				synchronized (this.jobs) {
					if (world.setSelection(world.getSelectionX() + 1, world.getSelectionY())) {
						this.updateCursor();
					}
				}
				break;
			//			case GLFW_KEY_R:
			//				this.camera.setRot(45, 0);
			//				break;
		}
	}

	private final void updateCursor() {
		final int x = world.getSelectionX();
		final int y = world.getSelectionY();
		this.camera.setPos(x, 10, 10 + y);
		this.cursor.setPos(x, 0, y);
	}
}