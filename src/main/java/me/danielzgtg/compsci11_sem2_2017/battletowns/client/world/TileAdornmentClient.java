package me.danielzgtg.compsci11_sem2_2017.battletowns.client.world;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

public abstract class TileAdornmentClient extends ClientObject {

	public final TileClient tile;
	private final List<String> actions = new LinkedList<>();

	public TileAdornmentClient(final TileClient tile) {
		super(tile.x, tile.y, tile.client);

		this.tile = tile;
	}

	public final TileClient getTile() {
		return tile;
	}

	@Override
	protected final void doRemove() {
		if (this instanceof UnitClient) {
			tile.setUnit(null);
		} else if (this instanceof BuildingClient) {
			tile.setBuilding(null);
		} else {
			throw new AssertionError();
		}

		this.removeAdornment();
	}

	protected abstract void removeAdornment();

	public final List<String> getActions() {
		return Collections.unmodifiableList(new ArrayList<>(actions));
	}

	@Override
	protected final void clientUpdate(final String data) {
		Validate.notNull(data);

		final String[] split = data.split(",");

		final int length = split.length;
		if (length < 3) {
			throw new IllegalArgumentException();
		}

		final int numActions1 = Integer.parseInt(split[length - 1]) + 1;

		if (numActions1 < 1) {
			throw new ArrayIndexOutOfBoundsException();
		}

		int i = 2;
		if (numActions1 > 1) {
			if (length <= numActions1) {
				throw new ArrayIndexOutOfBoundsException();
			}

			this.actions.clear();

			for (; i <= numActions1; i++) {
				this.actions.add(split[length - i]);
			}
		}

		final int max = length - Math.max(numActions1, 2);
		final StringBuilder result = new StringBuilder();
		for (int j = 0;;) {
			result.append(split[j]);

			if (++j >= max) {
				break;
			} else {
				result.append(',');
			}
		}

		this.adornmentUpdate(result.toString());
	}

	protected abstract void adornmentUpdate(final String data);
}
