package me.danielzgtg.compsci11_sem2_2017.battletowns.client.world;

import javax.swing.JPanel;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.GameClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.util.Locatable;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Event;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;

public abstract class ClientObject implements Locatable {

	public final int x;
	public final int y;
	public GameClient client;
	/*packaged*/ int health;
	/*packaged*/ int maxHealth;
	/*packaged*/ String owner;

	public final Event<Void> removeEvent = new Event<>();
	public final Event<Void> updateEvent = new Event<>();

	public ClientObject(final int x, final int y, final GameClient client) {
		this.x = x;
		this.y = y;
		this.client = client;
	}

	@Override
	public final int getX() {
		return this.x;
	}

	@Override
	public final int getY() {
		return this.y;
	}

	public final int getHealth() {
		return health;
	}

	public final int getMaxHealth() {
		return maxHealth;
	}

	public final String getOwner() {
		return owner;
	}

	/*packaged*/ final void update(final String data) {
		this.clientUpdate(data);
		updateEvent.fire(null);
	}

	protected abstract void clientUpdate(final String data);

	public final void remove() {
		this.doRemove();
		this.removeEvent.fire(null);
	}

	protected abstract void doRemove();

	public final void requestInfo(final Widgets widgets, final JPanel clientPanel) {
		Validate.notNull(widgets);
		Validate.notNull(clientPanel);

		this.doRequestInfo(widgets, clientPanel);
	}

	protected void doRequestInfo(final Widgets widgets, final JPanel clientPanel) {}
}
