package me.danielzgtg.compsci11_sem2_2017.battletowns.client.world;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.GameClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.NetworkEquivalents;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.WorldCommon;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Event;

import static me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants.DEBUG;

@SuppressWarnings("SuspiciousMethodCalls")
public final class WorldClient extends WorldCommon {

	private final TileClient[][] tiles;
	public final GameClient client;
	private volatile int selectX;
	private volatile int selectY;
	public final Event<Void> selectionChangedEvent = new Event<>();

	public WorldClient(final int xSize, final int ySize, final GameClient client) {
		super(xSize, ySize);

		this.tiles = new TileClient[xSize][ySize];
		this.client = client;
	}

	public final int getSelectionX() {
		return this.selectX;
	}

	public final int getSelectionY() {
		return this.selectY;
	}

	public final boolean setSelection(final int x, final int y) {
		if (!this.isValidPosition(x, y)) {
			return false;
		}

		this.selectX = x;
		this.selectY = y;

		selectionChangedEvent.fire(null);

		return true;
	}

	public final TileClient getTile(final int x, final int y) {
		if (!this.isValidPosition(x, y)) {
			throw new ArrayIndexOutOfBoundsException();
		}

		return this.tiles[x][y];
	}

	public final void update(final TileNetwork data) {
		Validate.notNull(data);

		try {
			final TileClient tile = this.updateTile(data);
			this.updateBuilding(data, tile);
			this.updateUnit(data, tile);
		} catch (final Exception e) {
			Log.log("[WorldClient] Error updating world!");
			Log.log(e);
			throw new RuntimeException(e);
		}
	}

	private final String updateClientObject(final String[] split, final ClientObject target) {
		Validate.notNull(split);
		Validate.notNull(target);

		final int length = split.length;
		if (length < 5) {
			throw new IllegalArgumentException();
		}

		target.maxHealth = Integer.parseInt(split[length - 1]);
		final String ownerStr = split[length - 2];
		target.owner = "system".equals(ownerStr) ? null : ownerStr;
		target.health = Integer.parseInt(split[length - 3]);

		final StringBuilder clientData = new StringBuilder();
		final int limit = length - 4;
		for (int i = 0; ; ) {
			clientData.append(split[i]);

			if (++i >= limit) {
				break;
			} else {
				clientData.append(',');
			}
		}

		return clientData.toString();
	}

	private final TileClient updateTile(final TileNetwork data) {
		final TileClient old = tiles[data.x][data.y];
		final String tileData = data.getTileData();

		if ("".equals(tileData)) {
			tiles[data.x][data.y] = null;
			Log.log(String.format("[WorldClient] Setting tile to null, this is not normal. (%d,%d)", data.x, data.y));
			Log.log("[WorldClient] Tile before setting to null is " + String.valueOf(old));
			return null;
		}

		final String[] split = tileData.split(",");

		final int length = split.length;
		if (length < 5) {
			throw new IllegalArgumentException();
		}

		if (DEBUG) {
			Log.log("[WorldClient] Replacing tile: " + String.valueOf(old));
		}

		final TileClient current;
		try {
			final Class<?> serverClass = Class.forName(split[length - 4]);
			final Class<? extends TileClient> newTileClass =
					NetworkEquivalents.TILES.get(serverClass);
			if (newTileClass == null) {
				throw new IllegalArgumentException("Unknown tile: " + serverClass.getName());
			}
			if (old == null || !old.getClass().equals(newTileClass)) {
				current = tiles[data.x][data.y] =
						newTileClass.getDeclaredConstructor(int.class, int.class, GameClient.class)
								.newInstance(data.x, data.y, this.client);
			} else {
				current = old;
			}
		} catch (final Exception e) {
			throw new IllegalArgumentException(e);
		}

		current.update(this.updateClientObject(split, current));

		if (DEBUG) {
			Log.log("[WorldClient] New tile: " + String.valueOf(current));
		}

		return current;
	}

	private final void updateBuilding(final TileNetwork data, final TileClient tile) {
		final String buildingData = data.getBuildingData();

		if ("".equals(buildingData)) {
			if (tile == null) {
				return;
			}

			final BuildingClient old = tile.getBuilding();

			if (old == null) {
				return;
			}

			if (DEBUG) {
				Log.log(String.format("[WorldClient] Removing building (%d,%d): %s", data.x, data.y, old));
			}

			old.remove();

			if (tile.getBuilding() != null) {
				throw new AssertionError();
			}

			return;
		}

		final String[] split = buildingData.split(",");

		final int length = split.length;
		if (length < 5) {
			throw new IllegalArgumentException();
		}

		if (tile == null) {
			throw new IllegalArgumentException();
		}

		final BuildingClient old = tile.getBuilding();

		if (DEBUG) {
			Log.log("[WorldClient] Replacing building: " + String.valueOf(old));
		}

		final BuildingClient current;
		try {
			final Class<?> serverClass = Class.forName(split[length - 4]);
			final Class<? extends BuildingClient> newBuildingClass =
					NetworkEquivalents.BUILDINGS.get(serverClass);
			if (newBuildingClass == null) {
				throw new IllegalArgumentException("Unknown building: " + serverClass.getName());
			}
			if (old == null || !old.getClass().equals(newBuildingClass)) {
				tile.setBuilding(current = newBuildingClass.getDeclaredConstructor(TileClient.class).newInstance(tile));
			} else {
				current = old;
			}
		} catch (final Exception e) {
			throw new IllegalArgumentException(e);
		}

		current.update(this.updateClientObject(split, current));

		if (DEBUG) {
			Log.log("[WorldClient] New building: " + String.valueOf(current));
		}
	}

	private final void updateUnit(final TileNetwork data, final TileClient tile) {
		final String unitData = data.getUnitData();

		if ("".equals(unitData)) {
			if (tile == null) {
				return;
			}

			final UnitClient old = tile.getUnit();

			if (old == null) {
				return;
			}

			if (DEBUG) {
				Log.log(String.format("[WorldClient] Removing unit (%d,%d): %s", data.x, data.y, old));
			}

			old.remove();

			if (tile.getUnit() != null) {
				throw new AssertionError();
			}

			return;
		}

		final String[] split = unitData.split(",");

		final int length = split.length;
		if (length < 5) {
			throw new IllegalArgumentException();
		}

		if (tile == null) {
			throw new IllegalArgumentException();
		}

		final UnitClient old = tile.getUnit();

		if (DEBUG) {
			Log.log("[WorldClient] Replacing unit: " + String.valueOf(old));
		}

		final UnitClient current;
		try {
			final Class<?> serverClass = Class.forName(split[length - 4]);
			final Class<? extends UnitClient> newUnitClass =
					NetworkEquivalents.UNITS.get(serverClass);
			if (newUnitClass == null) {
				throw new IllegalArgumentException("Unknown unit: " + serverClass.getName());
			}
			if (old == null || !old.getClass().equals(newUnitClass)) {
				tile.setUnit(current = newUnitClass.getDeclaredConstructor(TileClient.class).newInstance(tile));
			} else {
				current = old;
			}
		} catch (final Exception e) {
			throw new IllegalArgumentException(e);
		}

		current.update(this.updateClientObject(split, current));

		if (DEBUG) {
			Log.log("[WorldClient] New unit: " + String.valueOf(current));
		}
	}

	public final void onDestroy() {
		for (int x = 0; x < this.xSize; x++) {
			final TileClient[] subtiles = this.tiles[x];
			for (int y = 0; y < this.ySize; y++) {
				final TileClient tile = subtiles[y];

				if (tile != null) {
					tile.remove();
				}
			}
		}
	}

	@Override
	public final void tick() {}
}
