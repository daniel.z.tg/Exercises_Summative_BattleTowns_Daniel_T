package me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.GameClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.IngameGraphics;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.TileClient;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDBRow;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Model;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Quad;

public final class TileSacredClient extends TileClient {

	private volatile MemoryDBRow<Model> row;

	private static final float[][][] VERTICES = new float[][][] {
			// Floor
			new float[][] {
					//          x,     y,     z,      r,    g,    b,    a,     u,    v
					new float[] {
							+0.0f, +0.0f, +0.0f,   1.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							+1.0f, +0.0f, +0.0f,   1.0f, 1.0f, 1.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							+1.0f, +0.0f, +1.0f,   1.0f, 1.0f, 1.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							+0.0f, +0.0f, +1.0f,   1.0f, 1.0f, 1.0f, 1.0f,  0.0f, 1.0f
					}
			},
			// Pillar 1
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.0f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.0f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.0f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.0f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							2.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							2.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.0f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.0f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							2.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							2.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.0f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.0f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			// Pillar 2
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							3.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							3.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							3.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							3.0f/5, +0.0f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							3.0f/5, +0.0f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							3.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							3.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							3.0f/5, +0.0f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.0f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							4.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							4.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.0f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.0f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							4.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							4.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.0f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							3.0f/5, +0.0f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							3.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			// Pillar 3
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							3.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							3.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							3.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							3.0f/5, +0.0f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							3.0f/5, +0.0f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							3.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							3.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							3.0f/5, +0.0f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.0f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							4.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							4.0f/5, +0.8f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.0f, 2.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.0f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							4.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							4.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.0f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							3.0f/5, +0.0f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							3.0f/5, +0.8f, 1.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			// Pillar 4
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.0f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.0f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.0f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.0f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							2.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							2.0f/5, +0.8f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.0f, 4.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.0f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							2.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							2.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							2.0f/5, +0.0f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.0f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +0.8f, 3.0f/5,   0.7f, 0.7f, 0.2f, 1.0f,  0.0f, 1.0f
					}
			},
	};

	public TileSacredClient(final int x, final int y, final GameClient client) {
		super(x, y, client);

		final Model model = Model.builder().quad()
				.quad().quad().quad().quad().quad()
				.quad().quad().quad().quad().quad()
				.quad().quad().quad().quad().quad()
				.quad().quad().quad().quad().quad()
				.build();
		model.setPos(x, 0, y);
		for (int i = 0; i < VERTICES.length; i++) {
			final Quad quad = ((Quad) model.getPrimitives().get(i));
			final float[][] primitiveVertices = VERTICES[i];
			for (int j = 0; j < primitiveVertices.length; j++) {
				quad.setVertex(primitiveVertices[j], j);
			}
		}

		final IngameGraphics graphics = client.getGraphics();
		synchronized (graphics.jobs) {
			graphics.jobs.add(() -> {
				model.reupload();
				model.flipBuffers();

				row = graphics.models.addFirst(model);
			});
		}
	}

	@Override
	protected void clientUpdate(final String data) {}

	@Override
	protected void tileRemove() {
		if (row != null) {
			final IngameGraphics graphics = client.getGraphics();
			synchronized (graphics.jobs) {
				graphics.jobs.add(() -> {
					row.getData().cleanup();
					row.remove();
				});
			}
		}
	}
}
