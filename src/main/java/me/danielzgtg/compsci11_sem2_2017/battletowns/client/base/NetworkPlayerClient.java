package me.danielzgtg.compsci11_sem2_2017.battletowns.client.base;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.GameClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.NetworkProtocol;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.io.IOStreamUtil;

import static me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants.DEBUG;

@SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
public final class NetworkPlayerClient extends PlayerClient {

	private final InputStream in;
	private final OutputStream out;
	private final Thread thread;
	private volatile boolean disconnected;
	private final List<Runnable> work = new LinkedList<>();

	public NetworkPlayerClient(final GameClient client, final InputStream in, final OutputStream out) {
		super(client);

		Validate.notNull(in);
		Validate.notNull(out);
		Validate.notNull(client);

		this.in = in;
		this.out = out;
		this.thread = new Thread(this::playerLogic);
	}

	@Override
	protected void doChat(final String message) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_CLIENT_CHAT);
					IOStreamUtil.writeString(this.out, message);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doMakeReady() {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_CLIENT_READY);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doPass() {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_CLIENT_PASS);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doRequestWorldSize(final int xSize, final int ySize) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_CLIENT_SIZEREQUEST);
					IOStreamUtil.writeInt(this.out, xSize);
					IOStreamUtil.writeInt(this.out, ySize);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doRequestWorldType(final String type) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					this.out.write(NetworkProtocol.PACKETID_CLIENT_TYPEREQUEST);
					IOStreamUtil.writeString(this.out, type);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doPerformAction(final int x, final int y, final boolean building, final String action) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					final OutputStream out = this.out;
					out.write(NetworkProtocol.PACKETID_CLIENT_ACTION);
					IOStreamUtil.writeInt(out, x);
					IOStreamUtil.writeInt(out, y);
					out.write(building ? 1 : 0);
					IOStreamUtil.writeString(out, action);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void doPerformBattle(final boolean simulation, final int unitX, final int unitY, final int targetX,
			final int targetY, final boolean move) {
		final List<Runnable> work = this.work;
		synchronized (work) {
			work.add(() -> {
				try {
					final OutputStream out = this.out;
					out.write(NetworkProtocol.PACKETID_CLIENT_BATTLE);
					out.write(simulation ? 1 : 0);
					IOStreamUtil.writeInt(out, unitX);
					IOStreamUtil.writeInt(out, unitY);
					IOStreamUtil.writeInt(out, targetX);
					IOStreamUtil.writeInt(out, targetY);
					out.write(move ? 1 : 0);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	@Override
	protected void handleReset() {
		this.client.handleReset();
	}

	@Override
	protected void doHandleBegin(final int xSize, final int ySize) {
		this.client.doHandleBegin(xSize, ySize);
	}

	@Override
	protected void doHandleTurn(final String playerName) {
		this.client.doHandleTurn(playerName);
	}

	@Override
	protected void doHandleUpdate(final TileNetwork update) {
		this.client.doHandleUpdate(update);
	}

	@Override
	protected void doHandleResource(final Resource resource, final int amount) {
		this.client.doHandleResource(resource, amount);
	}

	@Override
	protected final void doDisconnect() {
		this.disconnected = true;
	}

	@Override
	public final void run() {
		this.thread.start();
	}

	private final void playerLogic() {
		this.client.program.addThread(Thread.currentThread(), "Client Network");

		try {
			try {
				if (DEBUG) {
					Log.log("[NetworkPlayerClient] Connected: " + this.toString());
				}

				long slept = 0;
				final List<Runnable> globalWork = this.work;
				final List<Runnable> tmpWork = new LinkedList<>();
				while (!this.disconnected) {
					if (this.in.available() != 0) {
						final byte id = (byte) this.in.read();

						this.handlePacket(id);
						slept = 0;
					}

					synchronized(globalWork) {
						tmpWork.addAll(globalWork);
						globalWork.clear();
					}

					for (final Iterator<Runnable> iter = tmpWork.iterator(); iter.hasNext();) {
						final Runnable job = iter.next();

						job.run();

						iter.remove();
					}

					if (tmpWork.size() != 0) {
						try {
							slept += NetworkProtocol.SLEEP_TIME;
							Thread.sleep(NetworkProtocol.SLEEP_TIME);
						} catch (final Exception ignore) {}

						if (slept > NetworkProtocol.KEEPALIVE_TIME) {
							this.out.write(NetworkProtocol.PACKETID_GLOBAL_KEEPALIVE);
							slept = 0;
						}
					}
				}
			} catch (final Exception e) {
				Log.log("[NetworkPlayerClient] Connection failed: " + this.toString());
				Log.log(e);
				this.disconnected = true;
				this.client.handleDisconnect(e.getMessage());
				return;
			}

			try {
				this.out.write(NetworkProtocol.PACKETID_GLOBAL_DISCONNECT);
			} catch (final Exception e) {
				Log.log("[NetworkPlayerClient] IO Error while disconnecting: " + this.toString());
			}
		} finally {
			if (DEBUG) {
				Log.log("[NetworkPlayerClient] Disconnecting: " + this.toString());
			}

			try {
				this.out.close();
				this.in.close();
			} catch (final Exception e) {
				Log.log("[NetworkPlayerClient] IO Error while closing: " + this.toString());
			}
		}
	}

	private final void handlePacket(final byte id) throws IOException {
		switch (id) {
			case NetworkProtocol.PACKETID_GLOBAL_KEEPALIVE:
				return;
			case NetworkProtocol.PACKETID_GLOBAL_DISCONNECT:
				this.client.handleDisconnect(IOStreamUtil.readString(this.in));
				return;
			case NetworkProtocol.PACKETID_SERVER_TURN:
				this.handleTurn(IOStreamUtil.readString(this.in));
				return;
			case NetworkProtocol.PACKETID_SERVER_UPDATE:
				this.handleUpdate(TileNetwork.deserialize(this.in));
				return;
			case NetworkProtocol.PACKETID_SERVER_BATTLE:
				this.client.receiveBattle(BattleResult.deserialize(this.in));
				return;
			case NetworkProtocol.PACKETID_SERVER_SIMULATION:
				this.client.receiveSimulation(BattleResult.deserialize(this.in));
				return;
			case NetworkProtocol.PACKETID_SERVER_RESOURCE: {
				final Resource resource = Resource.RESOURCE_LIST.get(this.in.read());
				final int amount = IOStreamUtil.readInt(this.in);
				this.handleResource(resource, amount);
				return;
			}
			case NetworkProtocol.PACKETID_SERVER_CHAT:
				this.client.receiveChat(IOStreamUtil.readString(this.in));
				return;
			case NetworkProtocol.PACKETID_SERVER_BEGIN:
				this.handleBegin(IOStreamUtil.readInt(this.in), IOStreamUtil.readInt(this.in));
				return;
			default:
				throw new IllegalArgumentException("Invalid Packet id");
		}
	}
}
