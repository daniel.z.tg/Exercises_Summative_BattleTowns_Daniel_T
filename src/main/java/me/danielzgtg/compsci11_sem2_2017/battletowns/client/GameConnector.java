package me.danielzgtg.compsci11_sem2_2017.battletowns.client;

import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Component;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleTownConfig;
import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.platform.MainThreadUtils;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;

public class GameConnector extends AppContainer {

	/**
	 * The initial width of the app's {@link JFrame}.
	 */
	private final int initialWindowWidth;

	/**
	 * The initial height of the app's {@link JFrame}.
	 */
	private final int initialWindowHeight;

	/**
	 * The {@link JPanel} of the content.
	 */
	private final JPanel contentPanel;

	/**
	 * The {@link JButton} to play singleplayer.
	 */
	private final JButton singlePlayerButton;

	public GameConnector() {
		// Setup consistent widgets
		final Widgets widgets =
				new Widgets(BattleTownConfig.BATTLETOWNS_CONSTANT_DATA, ResourceUtils.COMMONLIB_RESOURCE_LOADER);

		// Content
		this.contentPanel = new JPanel();
		SwingUtils.setFlowDirection(this.contentPanel, false);

		{
			this.singlePlayerButton = widgets.createRegularButton(String.valueOf(
					BattleTownConfig.BATTLETOWNS_CONSTANT_DATA
							.get("me.danielzgtg.compsci11_sem2_2017.battletowns.joingame.singleplayer")));

			this.contentPanel.add(this.singlePlayerButton);
		}

		// Load Dimensions
		this.initialWindowWidth = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE,
				Coerce.asInt(BattleTownConfig.BATTLETOWNS_CONSTANT_DATA.get(
						"me.danielzgtg.compsci11_sem2_2017.battletowns.joingame.initialWindowWidth")));
		this.initialWindowHeight = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE,Coerce.asInt(
				BattleTownConfig.BATTLETOWNS_CONSTANT_DATA.get(
						"me.danielzgtg.compsci11_sem2_2017.battletowns.joingame.initialWindowHeight")));
	}

	@Override
	protected void doLaunch() {
		MainThreadUtils.require();

		final JFrame frame = SwingUtils.generateOnetimeJFrame(BattleTownConstants.JOINGAME_TITLE);

		SwingUtils.hookButtonPress(this.singlePlayerButton, () -> {
			GameLauncher.singleplayer();
			MainThreadUtils.release();
			frame.dispose();
		});

		SwingUtils.setupJFrameContents(frame, new Component[] {
				this.contentPanel
		}, (panel) -> new BoxLayout(panel, BoxLayout.Y_AXIS));

		SwingUtils.finishJFrameSetup(frame, this.initialWindowWidth, this.initialWindowHeight);

		SwingUtils.hookJFrameClosing(frame, () -> {
			MainThreadUtils.release();

			return true;
		});

		SwingUtils.launchJFrame(frame);
	}
}
