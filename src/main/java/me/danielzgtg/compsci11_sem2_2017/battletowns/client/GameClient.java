package me.danielzgtg.compsci11_sem2_2017.battletowns.client;

import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Function;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import java.awt.BorderLayout;
import java.awt.Component;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.base.PlayerClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.BuildingClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.ClientObject;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.TileAdornmentClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.TileClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.UnitClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.WorldClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleResult;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.BattleTownConfig;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.battletowns.common.TileNetwork;
import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.platform.MainThreadUtils;
import me.danielzgtg.compsci11_sem2_2017.common.program.Program;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;

import static me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants.DEBUG;

@SuppressWarnings("SynchronizeOnNonFinalField")
public final class GameClient extends AppContainer {

	private final PlayerClient player;
	private WorldClient world;
	private BooleanSupplier shutdown;
	private volatile boolean stopped;
	public final Program program;

	private IngameGraphics graphics;
	private Thread graphicsThread;

	private final JPanel chatPanel;
	private final JButton chatSendButton;
	private final JTextArea chatArea;
	private final Widgets widgets;
	private final JPanel chatFlow;

	private final JPanel settingPanel;

	private final JPanel statePanel;
	private final JLabel turnLabel;
	private final JLabel[] resourceLabels = new JLabel[Resource.RESOURCE_LIST.size()];

	private final JPanel targetPanel;
	private final JLabel altLabel;
	private final JPanel targetPanelData;
	private int altSelectionX;
	private int altSelectionY;

	public GameClient(final Function<GameClient, PlayerClient> playerFunc, final Program program) {
		Validate.notNull(playerFunc);
		Validate.notNull(program);

		this.player = playerFunc.apply(this);
		this.program = program;

		if (this.player.getClient() != this) {
			throw new IllegalArgumentException();
		}

		this.widgets =
				new Widgets(BattleTownConfig.BATTLETOWNS_CONSTANT_DATA, BattleTownConfig.BATTLETOWNS_RESOURCE_LOADER);

		//UI
		{
			this.chatFlow = new JPanel();
			SwingUtils.setFlowDirection(this.chatFlow, false);

			this.chatPanel = new JPanel();
			SwingUtils.setFlowDirection(this.chatPanel, false);

			this.chatPanel.add(SwingUtils.wrapInGrowingJPanel(SwingUtils.wrapWithScrolling(this.chatFlow)));

			this.chatArea = new JTextArea();
			this.chatPanel.add(this.chatArea);

			this.chatSendButton = this.widgets.createRegularButton("Send");
			this.chatPanel.add(this.chatSendButton);

			SwingUtils.hookButtonPress(this.chatSendButton, () -> {
				this.player.chat(this.chatArea.getText());
				this.chatArea.setText("");
			});
		}

		{
			this.settingPanel = new JPanel();
			SwingUtils.setFlowDirection(this.settingPanel, false);

			final SpinnerNumberModel widthModel =
					new SpinnerNumberModel(BattleTownConstants.WORLD_MINLENGTH, null, null, 1);
			{
				this.settingPanel.add(this.widgets.createRegularLabel("World Width"));
				final JSpinner widthSpinner = new JSpinner(widthModel);
				this.settingPanel.add(widthSpinner);
			}

			final SpinnerNumberModel lengthModel =
					new SpinnerNumberModel(BattleTownConstants.WORLD_MINLENGTH, null, null, 1);
			{
				this.settingPanel.add(this.widgets.createRegularLabel("World Length"));
				final JSpinner lengthSpinner = new JSpinner(lengthModel);
				this.settingPanel.add(lengthSpinner);
			}

			final JButton worldDimensionButton = this.widgets.createRegularButton("Update Dimensions");

			SwingUtils.hookButtonPress(worldDimensionButton, () -> {
				this.player.requestWorldSize(widthModel.getNumber().intValue(), lengthModel.getNumber().intValue());
			});

			this.settingPanel.add(worldDimensionButton);
			this.settingPanel.add(this.widgets.createSimpleSpacer(true));

			final JTextArea typeArea = new JTextArea();
			this.settingPanel.add(typeArea);

			final JButton worldTypeButton = this.widgets.createRegularButton("Update Type");

			SwingUtils.hookButtonPress(worldTypeButton, () -> {
				this.player.requestWorldType(typeArea.getText());
			});

			this.settingPanel.add(worldTypeButton);
			this.settingPanel.add(widgets.createSimpleSpacer(true));

			final JButton readyButton = widgets.createRegularButton("Ready");

			SwingUtils.hookButtonPress(readyButton, this.player::makeReady);

			this.settingPanel.add(readyButton);
		}

		{
			this.statePanel = new JPanel();
			SwingUtils.setFlowDirection(this.statePanel, false);

			turnLabel = widgets.createRegularLabel("Turn");

			this.statePanel.add(turnLabel);

			final JButton passButton = widgets.createRegularButton("Pass");

			SwingUtils.hookButtonPress(passButton, this.player::pass);

			this.statePanel.add(passButton);

			final JPanel resourceContainer = new JPanel();
			SwingUtils.setFlowDirection(resourceContainer, false);

			final int length = resourceLabels.length;
			for (int i = 0; i < length; i++) {
				final JLabel label = widgets
						.createRegularLabel("Loading resource: " + Resource.RESOURCE_LIST.get(i).name());
				resourceLabels[i] = label;
				resourceContainer.add(label);
			}

			this.statePanel.add(SwingUtils.wrapWithScrolling(resourceContainer));
		}

		{
			this.targetPanel = new JPanel();
			SwingUtils.setFlowDirection(this.targetPanel, false);

			altLabel = widgets.createRegularLabel("Loading Selection...");
			this.targetPanel.add(altLabel);

			final JButton copySelectionButton = widgets.createRegularButton("Copy Selection to Alt Selection");

			SwingUtils.hookButtonPress(copySelectionButton, () -> {
				this.altSelectionX = this.world.getSelectionX();
				this.altSelectionY = this.world.getSelectionY();
				this.refreshSelection();
			});

			this.targetPanel.add(copySelectionButton);

			targetPanelData = new JPanel();
			SwingUtils.setFlowDirection(targetPanelData, false);

			this.targetPanel.add(SwingUtils.wrapWithScrolling(targetPanelData));
		}
	}

	private final void refreshSelection() {
		final int x = world.getSelectionX();
		final int y = world.getSelectionY();
		final String text = String
				.format("Main Selection (%d,%d), Alt Selection (%d,%d)", x, y, altSelectionX, altSelectionY);

		final JPanel tilePanel = new JPanel();
		SwingUtils.setFlowDirection(tilePanel, false);
		SwingUtils.setTitle(tilePanel, "Tile");

		final TileClient tile = this.world.getTile(x, y);

		if (tile == null) {
			return;
		}

		this.refreshClientObject(tilePanel, tile);

		final UnitClient unit = tile.getUnit();

		final JPanel unitPanel;
		if (unit != null) {
			unitPanel = new JPanel();
			SwingUtils.setFlowDirection(unitPanel, false);
			SwingUtils.setTitle(unitPanel, "Unit");

			this.refreshClientObject(unitPanel, unit);
		} else {
			unitPanel = null;
		}

		final BuildingClient building = tile.getBuilding();

		final JPanel buildingPanel;
		if (building != null) {
			buildingPanel = new JPanel();
			SwingUtils.setFlowDirection(buildingPanel, false);
			SwingUtils.setTitle(buildingPanel, "Building");

			this.refreshClientObject(buildingPanel, building);
		} else {
			buildingPanel = null;
		}

		SwingUtilities.invokeLater(() -> {
			altLabel.setText(text);

			targetPanelData.removeAll();
			targetPanelData.revalidate();
			targetPanelData.repaint();
			targetPanelData.add(tilePanel);

			if (unitPanel != null) {
				targetPanelData.add(unitPanel);
			}

			if (buildingPanel != null) {
				targetPanelData.add(buildingPanel);
			}

			targetPanel.validate();
		});
	}

	private final void refreshClientObject(final JPanel clientPanel, final ClientObject obj) {
		Validate.notNull(clientPanel);
		Validate.notNull(obj);

		clientPanel.add(widgets.createRegularLabel("Type: " + obj.getClass().getSimpleName()));
		clientPanel.add(widgets.createRegularLabel("Health: " + obj.getHealth()));
		clientPanel.add(widgets.createRegularLabel("Max Health: " + obj.getMaxHealth()));
		final String owner = obj.getOwner();
		clientPanel.add(widgets.createRegularLabel("Owner: " + (owner == null ? "system" : owner)));

		obj.requestInfo(widgets, clientPanel);

		if (obj instanceof TileAdornmentClient) {
			final TileAdornmentClient adornmentClient = (TileAdornmentClient) obj;
			final List<String> actions = adornmentClient.getActions();

			for (final String action : actions) {
				final JButton button = widgets.createRegularButton(action);

				SwingUtils.hookButtonPress(button, () -> {
					this.player.performAction(obj.x, obj.y, obj instanceof BuildingClient, action);
				});

				clientPanel.add(button);
			}

			if (obj instanceof UnitClient) {
				{
					final JButton button = widgets.createRegularButton("Move");

					SwingUtils.hookButtonPress(button, () -> {
						this.player.performBattle(
								false, obj.x, obj.y, altSelectionX, altSelectionY, true);
					});

					clientPanel.add(button);
				}

				{
					final JButton button = widgets.createRegularButton("Attack");

					SwingUtils.hookButtonPress(button, () -> {
						this.player.performBattle(
								false, obj.x, obj.y, altSelectionX, altSelectionY, false);
					});

					clientPanel.add(button);
				}
			}
		}
	}

	public final PlayerClient getPlayer() {
		return this.player;
	}

	public final WorldClient getWorld() {
		return this.world;
	}

	public final IngameGraphics getGraphics() {
		return this.graphics;
	}

	@Override
	protected void doLaunch() {
		MainThreadUtils.require();
		new Thread(this::gameLogic).start();
	}

	private JFrame worldSettingsFrame;
	private JFrame stateFrame;
	private JFrame targetFrame;

	private final void gameLogic() {
		this.player.run();

		final JFrame chatFrame = SwingUtils.generateOnetimeJFrame("Chat");
		this.worldSettingsFrame = SwingUtils.generateOnetimeJFrame("World Settings");
		this.stateFrame = SwingUtils.generateOnetimeJFrame("Game State");
		this.targetFrame = SwingUtils.generateOnetimeJFrame("World Target");

		this.shutdown = () -> {
			synchronized (this.shutdown) {
				if (this.stopped) {
					return true;
				}

				this.stopped = true;
			}

			this.player.disconnect();

			final IngameGraphics graphics = this.graphics;

			if (graphics != null) {
				graphics.close();
			}

			chatFrame.dispose();
			targetFrame.dispose();
			this.worldSettingsFrame.dispose();
			this.stateFrame.dispose();

			if (graphics != null) {
				graphics.fullStop();

				boolean interrupted = false;
				Thread graphicsThread;
				while ((graphicsThread  = this.graphicsThread) != null) {
					try {
						graphicsThread.join();
					} catch (final InterruptedException e) {
						interrupted = true;
					}
				}

				if (interrupted) {
					Thread.currentThread().interrupt();
				}
			}

			GameLauncher.mainMenu();

			MainThreadUtils.release();

			return true;
		};

		// Chat
		{
			SwingUtils.setupJFrameContents(chatFrame, new Component[] {
					this.chatPanel
			}, (panel) -> new BorderLayout());

			SwingUtils.finishJFrameSetup(chatFrame, 640, 480);
			SwingUtils.launchJFrame(chatFrame);
			SwingUtils.hookJFrameClosing(chatFrame, this.shutdown);
		}

		// World Settings
		{
			SwingUtils.setupJFrameContents(worldSettingsFrame, new Component[] {
					this.settingPanel
			}, (panel) -> new BorderLayout());

			SwingUtils.finishJFrameSetup(worldSettingsFrame, 320, 480);
			SwingUtils.launchJFrame(worldSettingsFrame);
			SwingUtils.hookJFrameClosing(worldSettingsFrame, this.shutdown);
		}

		// World State
		{
			SwingUtils.setupJFrameContents(stateFrame, new Component[] {
					this.statePanel
			}, (panel) -> new BorderLayout());

			SwingUtils.finishJFrameSetup(stateFrame, 320, 480);
			stateFrame.pack();
			//			SwingUtils.launchJFrame(stateFrame);
			SwingUtils.hookJFrameClosing(stateFrame, this.shutdown);
			this.stateFrame.setVisible(false);
		}

		// World Target
		{
			SwingUtils.setupJFrameContents(targetFrame, new Component[] {
					this.targetPanel
			}, (panel) -> new BorderLayout());

			SwingUtils.finishJFrameSetup(targetFrame, 480, 520);
			targetFrame.setLocationRelativeTo(null);
			SwingUtils.hookJFrameClosing(targetFrame, this.shutdown);
		}
	}

	public final void receiveSimulation(final BattleResult battleResult) {
		this.requireWorld();
		//TODO
	}

	public final void receiveBattle(final BattleResult battleResult) {
		this.requireWorld();
		//TODO
	}

	public final void receiveChat(final String message) {
		Log.log("[GameClient] ReceiveChat: " + message);
		SwingUtilities.invokeLater(() -> {
			Log.log("[GameClient]: UpdateChat: " + message);
			this.chatFlow.add(this.widgets.createRegularLabel(message));
			this.chatFlow.validate();
		});
	}

	private final void runGraphics() {
		if (DEBUG) {
			Log.log("[GameClient] RunGraphics");
		}

		if (this.graphics != null || this.graphicsThread != null) {
			program.stop();
			throw new IllegalStateException();
		}

		boolean[] ready = new boolean[1];
		graphicsThread = new Thread(() -> {
			final IngameGraphics graphics = this.graphics = new IngameGraphics(this.program, this.world);
			ready[0] = true;
			graphics.run();
			this.graphics = null;
			this.graphicsThread = null;

			if (!this.program.running()) {
				this.shutdown.getAsBoolean();
			}
		});
		graphicsThread.start();

		boolean interrupted = false;
		while (!ready[0]) {
			try {
				Thread.sleep(1L);
			} catch (final InterruptedException e) {
				interrupted = true;
			}
		}

		if (interrupted) {
			Thread.currentThread().interrupt();
		}
	}

	public final void handleReset() {
		this.altSelectionX = 0;
		this.altSelectionY = 0;

		this.worldSettingsFrame.setVisible(true);
		this.stateFrame.setVisible(false);
		this.targetFrame.setVisible(false);

		if (this.world != null) {
			this.world.onDestroy();
			this.world = null;
		}

		final IngameGraphics graphics = this.graphics;
		if (graphics != null) {
			graphics.close();
			graphics.fullStop();
			this.graphics = null;
		}
	}

	public final void doHandleBegin(final int xSize, final int ySize) {
		this.altSelectionX = 0;
		this.altSelectionY = 0;

		this.worldSettingsFrame.setVisible(false);
		this.stateFrame.setVisible(true);
		this.targetFrame.setVisible(true);

		if (this.world != null || this.graphics != null) {
			throw new IllegalStateException();
		}

		this.world = new WorldClient(xSize, ySize, this);
		this.world.selectionChangedEvent.addListener((ignore) -> this.refreshSelection());
		this.refreshSelection();
		this.runGraphics();
	}

	public final void doHandleUpdate(final TileNetwork update) {
		this.requireWorld();

		this.world.update(update);
		this.refreshSelection();
	}

	public final void doHandleTurn(final String playerName) {
		final String text = "It is " + ("".equals(playerName) ? "your" : playerName + "'s") + " turn.";
		Log.log("[GameClient] HandleTurn: " + text);

		SwingUtilities.invokeLater(() -> {
			this.turnLabel.setText(text);
			this.statePanel.validate();
		});
	}

	public final void doHandleResource(final Resource resource, final int amount) {
		final String text = resource.name() + ": " + amount;
		Log.log("[GameClient] HandleResource: " + text);

		SwingUtilities.invokeLater(() -> {
			this.resourceLabels[resource.ordinal()].setText(text);
			this.statePanel.validate();
		});
	}

	public final void handleDisconnect(final String reason) {
		Validate.notNull(reason);

		Log.log("[GameClient] HandleDisconnect: " + reason);

		JOptionPane.showMessageDialog(null, reason);
		this.shutdown.getAsBoolean();
	}

	private final void requireWorld() {
		if (this.world == null) {
			throw new IllegalStateException("A world is required for this action!");
		}
	}
}
