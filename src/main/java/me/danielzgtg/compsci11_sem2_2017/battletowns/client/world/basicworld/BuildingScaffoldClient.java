package me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld;

import javax.swing.JPanel;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.IngameGraphics;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.BuildingClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.TileClient;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDBRow;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Model;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Quad;

public final class BuildingScaffoldClient extends BuildingClient {

	private volatile MemoryDBRow<Model> row;

	private int ticksLeft;
	private int totalTicks;
	private String buildingName;

	private static final float[][][] VERTICES = new float[][][] {
			new float[][] {
					//          x,     y,     z,      r,    g,    b,    a,     u, v
					new float[] {
							+0.0f, +1.0f, +0.0f,   1.0f, 0.5f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							+1.0f, +1.0f, +0.0f,   1.0f, 0.5f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							+1.0f, +1.0f, +1.0f,   1.0f, 0.5f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							+0.0f, +1.0f, +1.0f,   1.0f, 0.5f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//          x,     y,     z,      r,    g,    b,    a,     u, v
					new float[] {
							+0.0f, +2.0f, +0.0f,   1.0f, 0.5f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							+1.0f, +2.0f, +0.0f,   1.0f, 0.5f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							+1.0f, +2.0f, +1.0f,   1.0f, 0.5f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							+0.0f, +2.0f, +1.0f,   1.0f, 0.5f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//          x,     y,     z,      r,    g,    b,    a,     u, v
					new float[] {
							+0.0f, +3.0f, +0.0f,   1.0f, 0.5f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							+1.0f, +3.0f, +0.0f,   1.0f, 0.5f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							+1.0f, +3.0f, +1.0f,   1.0f, 0.5f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							+0.0f, +3.0f, +1.0f,   1.0f, 0.5f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			}
	};

	public BuildingScaffoldClient(final TileClient tile) {
		super(tile);

		final Model model = Model.builder()
				.quad().quad().quad()
				.build();
		model.setPos(x, 0, y);
		for (int i = 0; i < VERTICES.length; i++) {
			final Quad quad = ((Quad) model.getPrimitives().get(i));
			final float[][] primitiveVertices = VERTICES[i];
			for (int j = 0; j < primitiveVertices.length; j++) {
				quad.setVertex(primitiveVertices[j], j);
			}
		}

		final IngameGraphics graphics = client.getGraphics();
		synchronized (graphics.jobs) {
			graphics.jobs.add(() -> {
				model.reupload();
				model.flipBuffers();

				row = graphics.models.addFirst(model);
			});
		}
	}

	@Override
	protected void removeAdornment() {
		if (row != null) {
			final IngameGraphics graphics = client.getGraphics();
			synchronized (graphics.jobs) {
				graphics.jobs.add(() -> {
					row.getData().cleanup();
					row.remove();
				});
			}
		}
	}

	@Override
	protected void adornmentUpdate(final String data) {
		final String[] split = data.split(",");

		if (split.length != 3) {
			throw new IllegalArgumentException();
		}

		this.ticksLeft = Integer.valueOf(split[0]);
		this.totalTicks = Integer.valueOf(split[1]);
		this.buildingName = split[2];
	}

	@Override
	protected void doRequestInfo(final Widgets widgets, final JPanel clientPanel) {
		clientPanel.add(widgets.createRegularLabel(
				"Building... (" + (this.totalTicks - this.ticksLeft) + "/" + this.totalTicks + ")"));
		clientPanel.add(widgets.createRegularLabel("Result will be: " + this.buildingName));
	}
}
