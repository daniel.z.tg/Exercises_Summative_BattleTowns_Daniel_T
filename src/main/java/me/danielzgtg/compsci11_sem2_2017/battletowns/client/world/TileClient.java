package me.danielzgtg.compsci11_sem2_2017.battletowns.client.world;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.GameClient;

public abstract class TileClient extends ClientObject {

	private BuildingClient building;
	private UnitClient unit;

	public TileClient(final int x, final int y, final GameClient client) {
		super(x, y, client);
	}

	public final BuildingClient getBuilding() {
		return building;
	}

	public final UnitClient getUnit() {
		return unit;
	}

	/*packaged*/ final void setBuilding(final BuildingClient building) {
		this.building = building;
	}

	/*packaged*/ final void setUnit(final UnitClient unit) {
		this.unit = unit;
	}

	@Override
	protected final void doRemove() {
		if (building != null) {
			this.building.remove();
		}

		if (unit != null) {
			this.unit.remove();
		}

		this.tileRemove();
	}

	protected abstract void tileRemove();
}
