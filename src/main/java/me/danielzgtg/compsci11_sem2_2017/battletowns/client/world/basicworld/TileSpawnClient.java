package me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.GameClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.IngameGraphics;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.TileClient;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDBRow;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Model;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Quad;

public final class TileSpawnClient extends TileClient {

	private volatile MemoryDBRow<Model> row;

	private static final float[][] VERTICES = new float[][] {
			//          x,     y,     z,      r,    g,    b,    a,     u, v
			new float[] {
					+0.0f, +0.0f, +0.0f,   1.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f
			},
			new float[] {
					+1.0f, +0.0f, +0.0f,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 0.0f
			},
			new float[] {
					+1.0f, +0.0f, +1.0f,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 1.0f
			},
			new float[] {
					+0.0f, +0.0f, +1.0f,   1.0f, 0.0f, 1.0f, 1.0f,  0.0f, 1.0f
			}
	};

	public TileSpawnClient(final int x, final int y, final GameClient client) {
		super(x, y, client);

		final Model model = Model.builder().quad().build();
		model.setPos(x, 0, y);
		final Quad quad = ((Quad) model.getPrimitives().get(0));
		for (int i = 0; i < VERTICES.length; i++) {
			quad.setVertex(VERTICES[i], i);
		}

		final IngameGraphics graphics = client.getGraphics();
		synchronized (graphics.jobs) {
			graphics.jobs.add(() -> {
				model.reupload();
				model.flipBuffers();

				row = graphics.models.addFirst(model);
			});
		}
	}

	@Override
	protected void clientUpdate(final String data) {}

	@Override
	protected void tileRemove() {
		if (row != null) {
			final IngameGraphics graphics = client.getGraphics();
			synchronized (graphics.jobs) {
				graphics.jobs.add(() -> {
					row.getData().cleanup();
					row.remove();
				});
			}
		}}
}
