package me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld;

import javax.swing.JPanel;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.IngameGraphics;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.BuildingClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.TileClient;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDBRow;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Model;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Quad;

public class BuildingAltarClient extends BuildingClient {

	private int winTicks;

	private volatile MemoryDBRow<Model> row;

	private static final float[][][] VERTICES = new float[][][] {
			// Lower Layer
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +1.3f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +1.3f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +1.3f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +1.3f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +1.3f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.8f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.8f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							4.0f/5, +1.3f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							4.0f/5, +1.3f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.8f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.8f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							4.0f/5, +1.3f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							4.0f/5, +1.3f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							4.0f/5, +0.8f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.8f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +1.3f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							1.0f/5, +1.3f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.8f, 4.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							1.0f/5, +0.8f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							1.0f/5, +1.3f, 1.0f/5,   0.0f, 1.0f, 1.0f, 1.0f,  0.0f, 1.0f
					}
			},
			// Top Layer
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							2.0f/5, +1.6f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							3.0f/5, +1.6f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							3.0f/5, +1.6f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							2.0f/5, +1.6f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							2.0f/5, +1.6f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							2.0f/5, +1.3f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							3.0f/5, +1.3f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							3.0f/5, +1.6f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							3.0f/5, +1.6f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							3.0f/5, +1.3f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							3.0f/5, +1.3f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							3.0f/5, +1.6f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							3.0f/5, +1.6f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							3.0f/5, +1.3f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							2.0f/5, +1.3f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							2.0f/5, +1.6f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
			new float[][] {
					//           x,     y,      z,      r,    g,    b,    a,     u,    v
					new float[] {
							2.0f/5, +1.6f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 0.0f
					},
					new float[] {
							2.0f/5, +1.3f, 3.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 0.0f
					},
					new float[] {
							2.0f/5, +1.3f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  1.0f, 1.0f
					},
					new float[] {
							2.0f/5, +1.6f, 2.0f/5,   1.0f, 1.0f, 0.0f, 1.0f,  0.0f, 1.0f
					}
			},
	};

	public BuildingAltarClient(final TileClient tile) {
		super(tile);

		final Model model = Model.builder()
				.quad().quad().quad().quad().quad()
				.quad().quad().quad().quad().quad()
				.build();
		model.setPos(x, 0, y);
		for (int i = 0; i < VERTICES.length; i++) {
			final Quad quad = ((Quad) model.getPrimitives().get(i));
			final float[][] primitiveVertices = VERTICES[i];
			for (int j = 0; j < primitiveVertices.length; j++) {
				quad.setVertex(primitiveVertices[j], j);
			}
		}

		final IngameGraphics graphics = client.getGraphics();
		synchronized (graphics.jobs) {
			graphics.jobs.add(() -> {
				model.reupload();
				model.flipBuffers();

				row = graphics.models.addFirst(model);
			});
		}
	}

	@Override
	protected void removeAdornment() {
		if (row != null) {
			final IngameGraphics graphics = client.getGraphics();
			synchronized (graphics.jobs) {
				graphics.jobs.add(() -> {
					row.getData().cleanup();
					row.remove();
				});
			}
		}
	}

	@Override
	protected void adornmentUpdate(final String data) {
		this.winTicks = Integer.valueOf(data);
	}

	@Override
	protected void doRequestInfo(final Widgets widgets, final JPanel clientPanel) {
		clientPanel.add(widgets.createRegularLabel("Owner will win in: " + this.winTicks));
	}
}
