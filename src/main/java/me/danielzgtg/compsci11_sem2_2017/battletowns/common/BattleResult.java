package me.danielzgtg.compsci11_sem2_2017.battletowns.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.base.WorldObject;
import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.io.IOStreamUtil;

public final class BattleResult {

	public final int attackerDamage;
	public final int victimDamage;
	public final int attackerX;
	public final int attackerY;
	public final TileLayer attackerLayer;
	public final int victimX;
	public final int victimY;
	public final TileLayer victimLayer;

	// positive & zero is to victim
	// negative is to attacker, but one greater than actual in magnitude
	public final List<Integer> attacks;

	public BattleResult(final WorldObject attacker, final WorldObject victim, final List<Integer> attacks) {
		this(attacker.getX(), attacker.getY(), victim.getX(), victim.getY(),
				attacks, attacker.getLayer(), victim.getLayer());
	}

	private BattleResult(final int attackerX, final int attackerY, final int victimX, final int victimY,
			final List<Integer> attacks, final TileLayer attackerLayer, final TileLayer victimLayer) {
		this.attacks = Collections.unmodifiableList(new ArrayList<>(attacks));

		int attackerDamage = 0;
		int victimDamage = 0;
		for (final Integer i : this.attacks) {
			if (i == null) {
				throw new IllegalArgumentException();
			}

			if (i == Integer.MAX_VALUE) {
				throw new IllegalArgumentException();
			}

			if (i < 0) {
				attackerDamage -= i + 1;
			} else /*if (i >= 0)*/ {
				victimDamage += i;
			}
		}

		this.attackerDamage = attackerDamage;
		this.victimDamage = victimDamage;

		this.attackerX = attackerX;
		this.attackerY = attackerY;
		this.victimX = victimX;
		this.victimY = victimY;

		this.attackerLayer = attackerLayer;
		this.victimLayer = victimLayer;
	}

	public final void serialize(final OutputStream out) throws IOException {
		Validate.notNull(out);

		IOStreamUtil.writeInt(out, this.attackerX);
		IOStreamUtil.writeInt(out, this.attackerY);
		IOStreamUtil.writeInt(out, this.victimX);
		IOStreamUtil.writeInt(out, this.victimY);

		out.write(this.attackerLayer.id);
		out.write(this.victimLayer.id);

		final List<Integer> attacks = this.attacks;
		IOStreamUtil.writeInt(out, attacks.size());

		for (final Integer i : attacks) {
			if (i == null) {
				throw new IllegalArgumentException();
			}

			IOStreamUtil.writeInt(out, i);
		}
	}

	public static final BattleResult deserialize(final InputStream in) throws IOException {
		final List<Integer> attacks = new LinkedList<>();

		final int attackerX = IOStreamUtil.readInt(in);
		final int attackerY = IOStreamUtil.readInt(in);
		final int victimX = IOStreamUtil.readInt(in);
		final int victimY = IOStreamUtil.readInt(in);

		final TileLayer attackerLayer = TileLayer.valueOf(in.read());
		final TileLayer victimLayer = TileLayer.valueOf(in.read());

		final int length = IOStreamUtil.readInt(in);
		for (int j = 0; j < length; j++) {
			attacks.add(IOStreamUtil.readInt(in));
		}

		return new BattleResult(attackerX, attackerY, victimX, victimY, attacks, attackerLayer, victimLayer);
	}

	@Override
	public String toString() {
		return "BattleResult: { "
				+ ", ax=" + this.attackerX
				+ ", ay=" + this.attackerY
				+ ", al=" + this.attackerLayer.name()
				+ ", vx=" + this.victimX
				+ ", vy=" + this.victimY
				+ ", vl=" + this.victimLayer.name()
				+ ", attackerDamage=" + this.attackerDamage
				+ ", victimDamage=" + this.victimDamage
				+ ", attacks=[" + StringUtils.formatList(this.attacks, ",")
				+ "] }";
	}
}
