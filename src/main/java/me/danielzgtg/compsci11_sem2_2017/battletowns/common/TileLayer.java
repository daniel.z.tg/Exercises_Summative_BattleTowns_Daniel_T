package me.danielzgtg.compsci11_sem2_2017.battletowns.common;

public enum TileLayer {

	UNIT(0),
	BUILDING(1),
	TILE(2),
	;

	public final int id;

	private TileLayer(final int id) {
		if (id != this.ordinal()) {
			throw new AssertionError();
		}

		if (((byte) id) != id) {
			// Assumption made in BattleResult
			throw new AssertionError();
		}

		this.id = id;
	}

	public final int getId() {
		return this.id;
	}

	public final boolean isAdornment() {
		return this != TILE;
	}

	public final boolean isMovable() {
		return this == UNIT;
	}

	public static final TileLayer valueOf(final int id) {
		final TileLayer result = valueOf0(id);

		if (result.id != id) {
			throw new AssertionError();
		}

		return result;
	}

	private static final TileLayer valueOf0(final int id) {
		switch (id) {
			case 0:
				return UNIT;
			case 1:
				return BUILDING;
			case 2:
				return TILE;
			default:
				throw new IllegalArgumentException();
		}
	}
}
