package me.danielzgtg.compsci11_sem2_2017.battletowns.common;

public final class NetworkProtocol {

	public static final byte PACKETID_GLOBAL_KEEPALIVE = 0;
	public static final byte PACKETID_GLOBAL_DISCONNECT = 1;

	public static final byte PACKETID_SERVER_TURN = 2;
	public static final byte PACKETID_SERVER_UPDATE = 3;
	public static final byte PACKETID_SERVER_BATTLE = 4;
	public static final byte PACKETID_SERVER_SIMULATION = 5;
	public static final byte PACKETID_SERVER_RESOURCE = 6;
	public static final byte PACKETID_SERVER_CHAT = 7;
	public static final byte PACKETID_SERVER_BEGIN = 8;

	public static final byte PACKETID_CLIENT_CHAT = 2;
	public static final byte PACKETID_CLIENT_READY = 3;
	public static final byte PACKETID_CLIENT_SIZEREQUEST = 4;
	public static final byte PACKETID_CLIENT_TYPEREQUEST = 5;
	public static final byte PACKETID_CLIENT_ACTION = 6;
	public static final byte PACKETID_CLIENT_BATTLE = 7;
	public static final byte PACKETID_CLIENT_PASS = 8;

	public static final long SLEEP_TIME = 20L;
	public static final long KEEPALIVE_TIME = 2000L;

	@Deprecated
	private NetworkProtocol() { throw new UnsupportedOperationException(); }
}
