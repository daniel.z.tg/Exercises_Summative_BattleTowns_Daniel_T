package me.danielzgtg.compsci11_sem2_2017.battletowns.common;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import java.io.FileOutputStream;
import java.io.InputStream;

import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.serialization.SerializationUtils;

/**
 * Holds Battle Towns' configs.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
@SuppressWarnings("WeakerAccess")
public final class BattleTownConfig {

	/**
	 * Constants for battle towns.
	 */
	public static final Map<?, ?> BATTLETOWNS_CONSTANT_DATA;

	/**
	 * Config for battle towns.
	 */
	public static final Map<Object, Object> BATTLETOWNS_CONFIG = new HashMap<>();

	/**
	 * The prefix for constants that are copied to the config initially.
	 */
	private static final String DEFAULT_MIGRATE_PREFIX = "battletowns.defaultOption.";

	/**
	 * The resource loader for battle towns.
	 */
	public static final Function<String, InputStream> BATTLETOWNS_RESOURCE_LOADER = ResourceUtils
			.getResourceLoaderForClass(BattleTownConfig.class);

	/**
	 * The name of the file that the file config is from.
	 */
	public static final String FILE_CONFIG_NAME = "battletown.config";

	static {
		BATTLETOWNS_CONSTANT_DATA = loadConstantsMap();

		for (final Map.Entry entry : BATTLETOWNS_CONSTANT_DATA.entrySet()) {
			final Object key = entry.getKey();

			if (key instanceof String) {
				final String strKey = (String) key;

				if (strKey.startsWith(DEFAULT_MIGRATE_PREFIX)) {
					BATTLETOWNS_CONFIG.put(strKey.substring(DEFAULT_MIGRATE_PREFIX.length()), entry.getValue());
				}
			}
		}

		loadConfig();

		Runtime.getRuntime().addShutdownHook(new Thread(BattleTownConfig::saveConfig));
	}

	/**
	 * Loads the file config.
	 */
	private static final void loadConfig() {
		Log.log("[BattleTownConfig] Loading file config.");

		try {
			final Map<?, ?> fileConfig =
					ResourceUtils.loadConstantsMap(FILE_CONFIG_NAME, ResourceUtils.EXTERNAL_RESOURCE_LOADER);
			BATTLETOWNS_CONFIG.putAll(fileConfig);
		} catch (final Exception e) {
			Log.log("[BattleTownConfig] Could not load config! Ignore this if it is the first launch.");
			return;
		}

		Log.log("[BattleTownConfig] Finished loading file config.");
	}

	/**
	 * Saves the file config.
	 */
	private static final void saveConfig() {
		Log.log("[BattleTownConfig] Saving file config.");

		try {
			SerializationUtils.encode(new FileOutputStream(FILE_CONFIG_NAME), BATTLETOWNS_CONFIG);
		} catch (final Exception e) {
			Log.log("[BattleTownConfig] Failed to save file config!");
		}

		Log.log("[BattleTownConfig] Finshed saving file config");
	}

	/**
	 * Loads the constants {@link Map},
	 * based off {@link ResourceUtils#COMMON_CONSTANT_DATA}, and the local constants resource.
	 *
	 * @return The constants {@link Map}.
	 */
	@SuppressWarnings("unchecked")
	private static final Map<?, ?> loadConstantsMap() {
		final Map<?, ?> localMap =
				ResourceUtils.loadConstantsMap("battletown.constants", BATTLETOWNS_RESOURCE_LOADER);
		final Map<?, ?> result = new HashMap<>();

		result.putAll((Map) ResourceUtils.COMMON_CONSTANT_DATA);
		result.putAll((Map) localMap);

		return Collections.unmodifiableMap(result);
	}
}
