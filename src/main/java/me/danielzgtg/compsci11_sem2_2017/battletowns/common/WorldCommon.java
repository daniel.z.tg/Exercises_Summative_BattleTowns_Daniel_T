package me.danielzgtg.compsci11_sem2_2017.battletowns.common;

import me.danielzgtg.compsci11_sem2_2017.battletowns.BattleTownConstants;
import me.danielzgtg.compsci11_sem2_2017.common.Tickable;

@SuppressWarnings("WeakerAccess")
public abstract class WorldCommon implements Tickable {

	public final int xSize;
	public final int ySize;

	public WorldCommon(final int xSize, final int ySize) {
		if (!isValidSize(xSize, ySize)) {
			throw new IllegalArgumentException();
		}

		this.xSize = xSize;
		this.ySize = ySize;
	}

	public static final boolean isValidSize(final int xSize, final int ySize) {
		return xSize >= BattleTownConstants.WORLD_MINLENGTH && ySize >= BattleTownConstants.WORLD_MINLENGTH &&
				xSize <= BattleTownConstants.WORLD_MAXLENGTH && ySize <= BattleTownConstants.WORLD_MAXLENGTH;
	}

	public final boolean isValidPosition(final int x, final int y) {
		return x >= 0 && y >= 0 && x < xSize && y < ySize;
	}

	public final int getXSize() {
		return this.xSize;
	}

	public final int getYSize() {
		return this.ySize;
	}
}
