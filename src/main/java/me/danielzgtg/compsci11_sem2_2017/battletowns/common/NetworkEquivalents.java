package me.danielzgtg.compsci11_sem2_2017.battletowns.common;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.BuildingClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.TileClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.UnitClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.BuildingAltarClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.BuildingBarracksClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.BuildingFarmClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.BuildingScaffoldClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.TileBarrenClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.TileSacredClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.TileSpawnClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.UnitBuilderClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.UnitPriestClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.client.world.basicworld.UnitSoldierClient;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.BuildingAltarServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.BuildingBarracksServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.BuildingFarmServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.BuildingScaffoldServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.TileBarrenServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.TileSacredServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.TileSpawnServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.UnitBuilderServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.UnitPriestServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.basicworld.UnitSoldierServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.BuildingServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.TileServer;
import me.danielzgtg.compsci11_sem2_2017.battletowns.server.world.tilecore.UnitServer;

public final class NetworkEquivalents {

	public static final Map<Class<? extends TileServer>, Class<? extends TileClient>> TILES;
	public static final Map<Class<? extends BuildingServer>, Class<? extends BuildingClient>> BUILDINGS;
	public static final Map<Class<? extends UnitServer>, Class<? extends UnitClient>> UNITS;

	static {
		{
			final Map<Class<? extends TileServer>, Class<? extends TileClient>> tmpMappings = new HashMap<>();

			tmpMappings.put(TileBarrenServer.class, TileBarrenClient.class);
			tmpMappings.put(TileSpawnServer.class, TileSpawnClient.class);
			tmpMappings.put(TileSacredServer.class, TileSacredClient.class);

			TILES = Collections.unmodifiableMap(tmpMappings);
		}

		{
			final Map<Class<? extends BuildingServer>, Class<? extends BuildingClient>> tmpMappings = new HashMap<>();

			tmpMappings.put(BuildingScaffoldServer.class, BuildingScaffoldClient.class);
			tmpMappings.put(BuildingFarmServer.class, BuildingFarmClient.class);
			tmpMappings.put(BuildingBarracksServer.class, BuildingBarracksClient.class);
			tmpMappings.put(BuildingAltarServer.class, BuildingAltarClient.class);

			BUILDINGS = Collections.unmodifiableMap(tmpMappings);
		}

		{
			final Map<Class<? extends UnitServer>, Class<? extends UnitClient>> tmpMappings = new HashMap<>();

			tmpMappings.put(UnitBuilderServer.class, UnitBuilderClient.class);
			tmpMappings.put(UnitSoldierServer.class, UnitSoldierClient.class);
			tmpMappings.put(UnitPriestServer.class, UnitPriestClient.class);

			UNITS = Collections.unmodifiableMap(tmpMappings);
		}
	}

	@Deprecated
	private NetworkEquivalents() { throw new UnsupportedOperationException(); }
}
