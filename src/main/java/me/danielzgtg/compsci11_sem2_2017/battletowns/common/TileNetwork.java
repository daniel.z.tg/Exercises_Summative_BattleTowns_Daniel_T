package me.danielzgtg.compsci11_sem2_2017.battletowns.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.io.IOStreamUtil;

@SuppressWarnings("WeakerAccess")
public final class TileNetwork {
	public final int x;
	public final int y;

	public final String tileData;
	public final String buildingData;
	public final String unitData;

	public TileNetwork(final int x, final int y,
			final String tileData, final String buildingData, final String unitData) {
		if (x < 0 || y < 0 || tileData == null || buildingData == null || unitData == null) {
			throw new IllegalArgumentException();
		}

		this.x = x;
		this.y = y;

		this.tileData = tileData;
		this.buildingData = buildingData;
		this.unitData = unitData;
	}

	public final int getX() {
		return this.x;
	}

	public final int getY() {
		return this.y;
	}

	public final String getTileData() {
		return this.tileData;
	}

	public final String getBuildingData() {
		return this.buildingData;
	}

	public final String getUnitData() {
		return this.unitData;
	}

	public final void serialize(final OutputStream out) throws IOException {
		Validate.notNull(out);

		IOStreamUtil.writeInt(out, this.x);
		IOStreamUtil.writeInt(out, this.y);

		IOStreamUtil.writeString(out, this.tileData);
		IOStreamUtil.writeString(out, this.buildingData);
		IOStreamUtil.writeString(out, this.unitData);
	}

	public static final TileNetwork deserialize(final InputStream in) throws IOException {
		Validate.notNull(in);

		final int x = IOStreamUtil.readInt(in);
		final int y = IOStreamUtil.readInt(in);

		final String tileData = IOStreamUtil.readString(in);
		final String buildingData = IOStreamUtil.readString(in);
		final String unitData = IOStreamUtil.readString(in);

		return new TileNetwork(x, y, tileData, buildingData, unitData);
	}

	@Override
	public String toString() {
		return "TileNetwork: { x=" + this.x + ", y=" + this.y
				+ ", tileData=\"" + this.tileData
				+ "\", buildingData=\"" + this.buildingData
				+ "\", unitData=\"" + this.unitData
				+ "\" }";
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof TileNetwork)) return false;
		final TileNetwork other = (TileNetwork) obj;

		return this.x == other.x && this.y == other.y && this.tileData.equals(other.tileData) &&
				this.buildingData.equals(other.buildingData) && this.unitData.equals(other.unitData);
	}

	@Override
	public int hashCode() {
		return this.x ^ this.y ^ this.tileData.hashCode() ^ this.buildingData.hashCode() ^ this.unitData.hashCode();
	}
}
