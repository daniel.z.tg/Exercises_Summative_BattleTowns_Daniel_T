package me.danielzgtg.compsci11_sem2_2017.battletowns.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Resource {

	FAITH,
	FOOD,
	LUMBER,
	;

	public static final List<Resource> RESOURCE_LIST = Collections.unmodifiableList(Arrays.asList(Resource.values()));
}
