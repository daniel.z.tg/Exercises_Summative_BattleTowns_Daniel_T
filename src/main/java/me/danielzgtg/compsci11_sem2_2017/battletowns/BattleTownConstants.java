package me.danielzgtg.compsci11_sem2_2017.battletowns;

import me.danielzgtg.compsci11_sem2_2017.battletowns.common.Resource;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

public class BattleTownConstants {

	// Misc
	public static final boolean DEBUG = true;
	public static final boolean QUICKDEBUG = false;
	public static final int LEADERBOARD_SLOTS = 10;
	public static final int MAX_LEADERBOARD_SLOTS = 25;

	// Server flags
	public static final int DEFAULT_PORT = 2017;

	// GUI Strings
	public static final String JOINGAME_TITLE = "Join Battle Towns";

	// Battle weapon
	public static final String WEAPON_TYPE_BUILDINGSCAFFOLD = "scaffold_sharps";
	public static final String WEAPON_TYPE_TERRAIN = "terrain_elements";
	public static final String WEAPON_TYPE_SACRED = "sacred_touch";
	public static final String WEAPON_TYPE_CIVIL = "civilian_riot";
	public static final String WEAPON_TYPE_SOLDIER = "total_war";

	// Battle health terrain
	public static final int HEALTH_DESERT = 5_000_000;
	public static final int HEALTH_LAKE = 2_000_000;

	// Battle health building
	public static final int HEALTH_BUILDINGSCAFFOLD = 2000;
	public static final int HEALTH_ALTAR = 20_000;
	public static final int HEALTH_FARM = 4000;
	public static final int HEALTH_BARRACKS = 8000;

	// Battle health unit
	public static final int HEALTH_BUILDER = 200;
	public static final int HEALTH_SOLDIER = 7000;
	public static final int HEALTH_PRIEST = 100;

	// Battle attack terrain
	public static final int ATTACK_DESERT = 200;
	public static final int ATTACK_LAKE = 500;

	// Battle attack building
	public static final int ATTACK_ALTAR = 2000;

	// Battle attack unit
	public static final int ATTACK_BUILDER = 500;
	public static final int ATTACK_SOLDIER = 6_500;
	public static final int ATTACK_PRIEST = 800;

	// Battle defense terrain
	public static final int DEFENSE_DESERT = 200;
	public static final int DEFENSE_LAKE = 500;

	// Battle defense building
	public static final int DEFENSE_BUILDINGSCAFFOLD = 50;
	public static final int DEFENSE_ALTAR = 300;
	public static final int DEFENSE_FARM = 100;
	public static final int DEFENSE_BARRACKS = 200;

	// Battle defense unit
	public static final int DEFENSE_BUILDER = 100;
	public static final int DEFENSE_SOLDIER = 1_000;
	public static final int DEFENSE_PRIEST = 90;

	// Battle priority
	public static final int PRIORITY_TERRAIN = -50;
	public static final int PRIORITY_SACRED = Integer.MIN_VALUE; // TODO
	public static final int PRIORITY_BUILDER = -20;
	public static final int PRIORITY_SOLDIER = 100;

	// Resource
	public static final int UPKEEP_ALTAR_FAITH = 10;
	public static final int UPKEEP_BARRACKS_FOOD = 3;
	public static final int UPKEEP_SOLDIER_FOOD = 1;
	public static final int UPKEEP_PRIEST_FOOD = 4;
	public static final int UPKEEP_SCAFFOLD_LUMBER = 1;
	public static final int PURCHASE_BUILDER_FOOD = QUICKDEBUG ? 1 : 20;
	public static final int PURCHASE_SOLDIER_FOOD = QUICKDEBUG ? 1 : 50;
	public static final int PURCHASE_PRIEST_FOOD = QUICKDEBUG ? 1 : 70;
	public static final int PURCHASE_ALTAR_FAITH = QUICKDEBUG ? 1 : 100;
	public static final int PURCHASE_FARM_LUMBER = QUICKDEBUG ? 1 : 10;
	public static final int PURCHASE_BARRACKS_LUMBER = QUICKDEBUG ? 1 : 20;
	public static final int GENERATION_FARM_FOOD = 2;
	public static final int GENERATION_FARM_FAITH = 1;
	public static final int GENERATION_FARM_LUMBER = 1;
	public static final int GENERATION_PRIEST_FAITH = 10;
	public static final int GENERATION_BUILDER_DEMOLISH_LUMBER = 5;

	// Building time
	public static final int BUILDTICKS_FARM = QUICKDEBUG ? 1 : 3;
	public static final int BUILDTICKS_BARRACKS = QUICKDEBUG ? 1 : 5;
	public static final int BUILDTICKS_ALTAR = QUICKDEBUG ? 1 : 7;

	// Game state
	public static final int WIN_TICKS = 16;

	// Calculated constants
	public static final int NUM_RESOURCES = Resource.values().length;

	// Server messages
	public static final String LEADERBOARD_HEADER = "~~ Recent winners (%d) ~~";
	public static final String LEADERBOARD_FORMAT = "* %s";
	public static final String REASON_UNKNOWN = "Unknown reason";
	public static final String RESTARTREASON_VICTORY = "%s has won this round!";
	public static final String RESTARTREASON_PLAYERRESET = "%s has reset the map!";
	public static final String RESTARTMSG = "This round has ended: %s";
	public static final String SHUTDOWNREASON_PLAYERSHUTDOWN = "%s stopping the server!";
	public static final String SHUTDOWN_KICKFORMAT = "being in the way of a server shutdown for %s";
	public static final String KICKREASON_DISCONNECTMSG = "You have been removed for %s.";
	public static final String KICKREASON_BROADCASTMSG = "%s has been removed for %s.";
	public static final String KICKREASON_PLAYERKICK = "offending %s";
	public static final String KICKREASON_CHAT = "chat violations";
	public static final String KICKREASON_HACKING = "hacking";
	public static final String KICKREASON_SELF = "leave requested";
	public static final String WORLDSETTINGCHANGE_TYPE = "%s changed the world type to %s.";
	public static final String WORLDSETTINGERROR_TYPE = "Invalid world type";
	public static final String WORLDSETTINGCHANGE_SIZE = "%s changed the world size to %d x %d.";
	public static final String WORLDSETTINGERROR_SIZE = "Invalid world size";
	public static final String VICTORYMSG = "Congratulations to %s for winning this round!";
	public static final String JOINMSG = "%s joined the game!";
	public static final String JOININTRODUCEMSG = "%s is online.";
	public static final String JOINNAMEFAILMSG = "Someone stole your identity!";
	public static final String BEGINMSG = "A new frontier awaits!";
	public static final String BEGINFAILPREFIX = "The round could not begin: ";
	public static final String SPAWNFAILMSG = "Unfortunately, %s could not spawn.";
	public static final String REJECTMSG_NOT_TURN = "It is not your turn. Hacking?";
	public static final String REJECTMSG_NOT_OWNER = "That tile adornment is not yours. Hacking?";
	public static final String REJECTMSG_NO_WORLD = "The game has ended! Hacking?";
	public static final String REJECTMSG_INVALID_ACTION = "That action is not supported! Hacking?";
	public static final String REJECTMSG_INVALID_POSITION = "That tile doesn't exist! Hacking?";
	public static final String REJECTMSG_EMPTY_LAYER = "The specified tile adornment doesn't exist! Hacking?";

	public static final String ACTIONMSG_DEMOLISH_NOTARGET = "There is nothing to demolish here!";
	public static final String ACTIONMSG_DEMOLISH_NOTOWNER = "Civilians can only demolish their owner's buildings!";
	public static final String ACTIONMSG_BUILDING_NOTEMPTY = "There is already a building here!";
	public static final String ACTIONMSG_UNIT_OCCUPIED = "The unit spawn area is occupied!";
	public static final String ACTIONMSG_MORE_RESOURCES = "You need more resources!";
	public static final String ACTIONMSG_UPKEEP_UNMET = "You lost some stuff due to a lack of upkeep!";

	// Chat
	public static final String CHAT_FORMAT = "[%s] %s";

	// Limits
	public static final int WORLD_MINLENGTH = 15;
	public static final int WORLD_MAXLENGTH = 20;
	public static final int CHAT_MAXLENGTH = 140;
	public static final String LIMITS_CHAT_FORMAT =
			"~~~BattleTown Limits~~~\n" +
					"World Dimension Min [%d], Max[%d]\n" +
					"Chat Message Max [%d]\n" +
					"Known World Types: [%s]";

	// Initial Resources
	private static final int[] DEFAULT_RESOURCES = new int[NUM_RESOURCES];

	static {
		DEFAULT_RESOURCES[Resource.LUMBER.ordinal()] = 100;
	}

	public static final void copyResources(final int[] target) {
		Validate.notNull(target);
		Validate.require(target.length == NUM_RESOURCES);

		System.arraycopy(DEFAULT_RESOURCES, 0, target, 0, NUM_RESOURCES);
	}

	@Deprecated
	private BattleTownConstants() { throw new UnsupportedOperationException(); }
}
