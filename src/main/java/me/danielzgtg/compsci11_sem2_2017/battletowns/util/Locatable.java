package me.danielzgtg.compsci11_sem2_2017.battletowns.util;

public abstract interface Locatable {

	public abstract int getX();

	public abstract int getY();
}
